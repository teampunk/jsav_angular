(function () {
    'use strict';

/**
 * @ngdoc overview
 * @name demoApp
 * @description
 * # demoApp
 *
 * Main module of the application.
 */
angular
    .module('jsav', [
        'jsav.auth',
        'ngAnimate',
        'ngAria',
        'ngMaterial',
        'jsav.config',
        'jsav.routes',
        'jsav.constants',
        'jsav.authentication',
        'jsav.home',
        'jsav.facilities',
        'jsav.conferences',
        'jsav.assistents',
        'jsav.speakers',
        'jsav.links',
        'jsav.downloads',
        'jsav.newsletters',
        'jsav.tips',
        'jsav.sponsors',

    ]);
    angular
        .module('jsav.routes', ['ngRoute','ui.router','ngResource','ngCookies','sasrio.angular-material-sidenav', 'ngAria', 'restangular','ngTouch', 'ngAnimate'] );

     angular
        .module('jsav.config', ['restangular', 'sasrio.angular-material-sidenav', 'ngSanitize','ngMessages','ngCookies', 'pascalprecht.translate', 'ngTouch', 'ngAnimate']);

    angular
        .module('jsav.constants', []);

    angular
        .module('jsav')
        .run(function($http, djangoAuth){
            $http.defaults.xsrfHeaderName = 'X-CSRFToken';
            $http.defaults.xsrfCookieName = 'csrftoken';
            djangoAuth.initialize('//localhost:8005/rest-auth', false);
        });
})();
(function () {
  'use strict';

  function config( 
            $mdThemingProvider,
            $locationProvider,
            ssSideNavSectionsProvider,
            $translateProvider,
            $httpProvider)
  {

          $translateProvider.useStaticFilesLoader({
            prefix: 'languages/locale-',
            suffix: '.json'
          });
          $translateProvider.preferredLanguage('es');
          // $translateProvider.useLocalStorage();
          $translateProvider.useCookieStorage();
          $translateProvider.useSanitizeValueStrategy('sanitize');

          $httpProvider.defaults.xsrfCookieName = 'csrftoken';
          $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

          $mdThemingProvider
              .theme('default')
              .primaryPalette('blue-grey', {
                  'default': '800',
                  //       'default': '400', // by default use shade 400 from the pink palette for primary intentions
                  //       'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                  //       'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                  //       'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
              });

    
            ssSideNavSectionsProvider.initWithTheme($mdThemingProvider);
            ssSideNavSectionsProvider.initWithSections([
              {
                id: 'inicio',
                name: 'INICIO',
                state: 'common.event',
                type: 'link',
            },
            {
                id: 'facilidades',
                name: 'FACILIDADES',
                state: 'common.facilidades',
                type: 'link',
            },
            {
                id: 'agenda',
                name: 'AGENDA',
                state: 'common.agenda',
                type: 'link',
            },{
                id: 'speakers',
                name: 'CONFERENCISTAS',
                state: 'common.speakers',
                type: 'link',
            },{
                id: 'assistents',
                name: 'ASISTENTES',
                state: 'common.assistents',
                type: 'link',
            },{
                id: 'links',
                name: 'LINKS',
                state: 'common.links',
                type: 'link',
            },{
                id: 'descargas',
                name: 'DESCARGAS',
                state: 'common.downloads',
                type: 'link',
            },{
                id: 'newsletters',
                name: 'BOLETINES',
                state: 'common.newsletters',
                type: 'link',
            },{
                id: 'tips',
                name: 'TIPS',
                state: 'common.tips',
                type: 'link',
            }, {
                id: 'patrocinadores',
                name: 'PATROCINADORES',
                state: 'common.sponsors',
                type: 'link',
            }, {
                id: 'ajustes',
                name: 'AJUSTES',
                state: 'common.ajustes',
                type: 'link'
            }, {
                id: 'logout',
                name: 'CERRAR_SESION',
                state: 'common.logout',
                type: 'link',
            }, 
        ]);
  }
   config.$inject = [
        '$mdThemingProvider',
        '$locationProvider',
        'ssSideNavSectionsProvider',
        '$translateProvider',
        '$httpProvider'
        ];
  
  angular
    .module('jsav.config')
      .config(config);
})();
(function () {
  'use strict';



  function config($urlRouterProvider, $stateProvider) {

          $urlRouterProvider.otherwise(function () {
                return '/login/';
            });
            $stateProvider.state({
                name: 'common',
                abstract: true,
                templateUrl: 'views/_common.html',
                controller: 'CommonCtrl'
            });
            $stateProvider.state({
                name: 'common.event',
                url: '/',
                templateUrl: 'views/home/home.html',
                controller: 'HomeController as vm',
            });


            $stateProvider.state({
                name: 'common.home',
                url: '/login/',
                templateUrl: 'views/authentication/login.html',
                controller: 'LoginController as vm'
                // controllerAs: 'vm',
            });
            $stateProvider.state({
                name: 'common.facilidades',
                url: '/facilidades/',
                templateUrl: 'views/facilidades/facilidades.html',
                controller: 'FacilitiesController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    },
                    facilities:function(Authentication, JsavRestangular){
                        var vm = this;
                        var event = Authentication.getAuthenticatedAccount();
                        vm.event_id = event.pk;
                        return JsavRestangular.one('events', vm.event_id).getList('facilities');

                    }
                }
            });
             $stateProvider.state({
                name: 'agenda',
                abstract:true,
                templateUrl: 'views/conferences/conferences.html',
                controller: 'CommonCtrl as vm',
                 resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                }
            });
            $stateProvider.state({
                name: 'common.agenda',
                url: '/agenda/',
                // parent: 'conferencias',
                templateUrl: 'views/conferences/conference-list.html',
                controller: 'ConferenceController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.detail',
                url: '/agenda/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/conferences/conferences-detail.html',
                controller: 'Detail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.speakers',
                url: '/conferencistas/',
                templateUrl: 'views/speakers/speakers-list.html',
                controller: 'SpeakersController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.speakersdetail',
                url: '/conferencistas/:id/',
                templateUrl: 'views/speakers/speakers-detail.html',
                controller: 'SpeakerDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.assistents',
                url: '/asistentes/',
                templateUrl: 'views/assistents/assistents-list.html',
                controller: 'AssistentsController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.asistentdetail',
                url: '/asistentes/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/assistents/assistents-detail.html',
                controller: 'AssistentDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.links',
                url: '/links/',
                templateUrl: 'views/links/links-list.html',
                controller: 'LinkController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.linkdetail',
                url: '/links/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/links/links-detail.html',
                controller: 'LinkDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });


            $stateProvider.state({
                name: 'common.downloads',
                url: '/descargas/',
                templateUrl: 'views/downloads/downloads-list.html',
                controller: 'DownloadController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.downloadsdetail',
                url: '/descargas/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/downloads/downloads-detail.html',
                controller: 'DownloadDetail as vm',
            });

            $stateProvider.state({
                name: 'common.newsletters',
                url: '/boletines/',
                templateUrl: 'views/newsletters/newsletters-list.html',
                controller: 'NewslettersController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.newslettersdetail',
                url: '/boletines/:id/',
                templateUrl: 'views/newsletters/newsletters-detail.html',
                controller: 'NewsletterDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });

            $stateProvider.state({
                name: 'common.tips',
                url: '/tips/',
                templateUrl: 'views/tips/tips-list.html',
                controller: 'TipsController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.tipsdetail',
                url: '/tips/:id/',
                templateUrl: 'views/tips/tips-detail.html',
                controller: 'TipDetailController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });

            $stateProvider.state({
                name: 'common.sponsors',
                url: '/patrocinadores/',
                templateUrl: 'views/sponsors/sponsors-list.html',
                controller: 'SponsorsController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.sponsorsdetail',
                url: '/patrocinadores/:id/',
                templateUrl: 'views/sponsors/sponsors-detail.html',
                controller: 'SponsorDetailController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });

          
            $stateProvider.state({
                name: 'common.ajustes',
                url: '/ajustes/',
                templateUrl: 'views/auth/login.html',
                controller:'LoginCtrl as vm',
                resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                      
                    }
            });

            $stateProvider.state({
                name: 'common.logout',
                resolve: {function(Authentication)
                    {
                        Authentication.unauthenticate();
                    }
                }
            });

                $stateProvider.state({
                    name: 'common.loginauth',
                    url: '/login-auth/',
                    templateUrl: 'views_auth/main.html',
                    controller: 'MainCtrl',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }]
                    }
                });
                $stateProvider.state({
                    name: 'common.register',
                    url: '/registro/',
                    templateUrl: 'views/auth/register.html',
                    controller: 'RegisterCtrl as vm',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                            return djangoAuth.authenticationStatus();
                        }],
                    }
                });
                $stateProvider.state({
                    name: 'common.passwordreset',
                    url: '/passwordReset',
                    templateUrl: 'views_auth/passwordReset.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.passwordResetConfirm',
                    url: '/passwordResetConfirm',
                    templateUrl: 'views_auth/passwordresetconfirm.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.login',
                    url: '/login-auth/login',
                    templateUrl: 'views_auth/login.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.emailVerificationToken',
                    url: '/verifyEmail/:emailVerificationToken',
                    templateUrl: 'views_auth/verifyemail.html',
                    resolve: {
                       authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.logoutauth',
                    url: '/logout-auth',
                    templateUrl: 'views_auth/logout.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.logout();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.userProfile',
                    url: '/userProfile',
                    templateUrl: 'views/auth/userprofile.html',
                    controller:'UserprofileCtrl as vm',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                        authenticated: ['djangoAuth', function(djangoAuth){
                        console.log(djangoAuth.authenticationStatus());
                      }],

                    }
                });
                $stateProvider.state({
                    name: 'common.passwordChange',
                    url: '/passwordChange',
                    controller: 'PasswordchangeCtrl as vm',
                    templateUrl: 'views/auth/passwordchange.html',
                    resolve: {
                       authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.restricted',
                    url: '/restricted',
                    controller: 'RestrictedCtrl',
                    templateUrl: 'views_auth/restricted.html',
                    resolve: {
                       authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.authRequired',
                    url: '/authRequired',
                    controller: 'AuthrequiredCtrl',
                    templateUrl: 'views_auth/authrequired.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus(true);
                      }],
                    }
                });                                
  

  
  }
  config.$inject = [ '$urlRouterProvider', '$stateProvider'];         
  
  angular
    .module('jsav.routes')
    .config(config);


})();

(function () {
	'use strict';

    var app = angular.module("jsav");

    var JsavConstants = function () {
        return {
                apiURL: '//localhost:8005/v1'
        };
    };

    app.factory("JsavConstants", JsavConstants);

}());
'use strict';

/**
 * @ngdoc function
 * @name demoApp.controller:CommonCtrl
 * @description
 * # CommonCtrl
 * Controller of the demoApp
 */
angular.module('jsav')
    .controller('CommonCtrl', [
        '$scope',
        '$mdSidenav',
        '$timeout',
        '$rootScope',
        '$state',
        'ssSideNav',
        'ssSideNavSharedService',
        'Authentication',
        '$translate',
        '$location',
        function (
            $scope,
            $mdSidenav,
            $timeout,
            $rootScope,
            $state,
            ssSideNav,
            ssSideNavSharedService,
            Authentication,
            $translate,
            $location

            ) {
            var vm = this;

            $scope.onClickMenu = function () {
                $mdSidenav('left').toggle();
            };

            $scope.menu = ssSideNav;

            $rootScope.event = Authentication.getAuthenticatedAccount();
            $scope.getLanguages = function(){
                Authentication.getLanguages();
             };

            $scope.setLanguages = function(selected_language){
                Authentication.setLanguages(selected_language);
            };

            $scope.changeLanguage = function (langKey) {
                $translate.use(langKey);
                $mdSidenav('left').toggle();
                $location.path('/');

            };
            
           
          
            

        }
    ]);
(function () {
  'use strict';

  angular
    .module('jsav.authentication', [
      'jsav.authentication.controllers',
      'jsav.authentication.services'
    ]);

  angular
    .module('jsav.authentication.controllers', ['JsavConstants', 'ngMessages']);

  angular
    .module('jsav.authentication.services', ['ngCookies']);
})();
/**
* LoginController
* @namespace thinkster.authentication.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.authentication.controllers', ['ngMessages'])
    .controller('LoginController', LoginController);
  	

  	LoginController.$inject = ['$location', '$scope', 'Authentication', 'Validate', '$mdDialog','$window', ];

    function LoginController($location, $scope, Authentication, Validate,$mdDialog, $window){
    	var vm = this;
    	vm.login = login;
    	vm.showAdvanced = showAdvanced;
    	// vm.cancel = cancel;
    	vm.object = {};


	    activate();
	/**
	    * @name activate
	    * @desc Actions to be performed when this controller is instantiated
	    * @memberOf thinkster.authentication.controllers.LoginController
	    */
	    function activate() {
			showAdvanced();
	      // If the user is authenticated, they should not be here.
	      if (!Authentication.isAuthenticated()) {
	        $location.path('/login');
	      }
	      else
	      {
	      	
	      	$location.path('/');	
	      }
	     
	    }

		     /**
	    * @name login
	    * @desc Log the user in
	    * @memberOf thinkster.authentication.controllers.LoginController
	    */
    function login (formData){
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
      if(!formData.$invalid){
	        Authentication.login(vm.event, vm.password, vm.email)
	        .then(function(data){
	        	$window.localStorage.setItem('idioma_enabled','false');
	        	// console.log("login idioma_enabled");
	          	// console.log($window.localStorage.getItem('idioma_enabled') );

	        },function(error){
	        	// error case
		    	if (error.status==404) {
		    		// console.log('data error 404 ');
		    	  	// console.log(error);
		    	  	vm.errors = error.data;
		    	}
	        });
      }
    }

    function showAdvanced (ev){
	    $mdDialog.show({
	      controller: DialogController,
	      templateUrl: 'views/dialog/dialog1.tmpl.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true,
	      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	      
	    })
	    .then(function(answer) {
	      $scope.status = 'You said the information was "' + answer + '".';
	    }, function() {
	      $scope.status = 'You cancelled the dialog.';
	    });
	  };

	  function DialogController($scope, $mdDialog) {
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	  };
	    
    }

})();
/**
* Authentication
* @namespace thinkster.authentication.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.authentication.services')
    .factory('Authentication', Authentication);

  Authentication.$inject = ['$q', '$cookies', '$http', 'JsavConstants', '$location','$window'];

  /**
  * @namespace Authentication
  * @returns {Factory}
  */
  function Authentication($q, $cookies, $http, JsavConstants, $location,$window) {
    /**
    * @name Authentication
    * @desc The Factory to be returned
    */
    var vm = this;
    var event = [];
    var apiURL = JsavConstants.apiURL;
    var Authentication = {
      getAuthenticatedAccount: getAuthenticatedAccount,
      isAuthenticated: isAuthenticated,
      login: login,
      logout: logout,
      // register: register,
      setAuthenticatedAccount: setAuthenticatedAccount,
      unauthenticate: unauthenticate,
      getLanguages:getLanguages,
      setLanguages:setLanguages,

    };

    return Authentication;
    /**
     * @name login
     * @desc Try to log in with email `email` and password `password`
     * @param {string} email The email entered by the user
     * @param {string} password The password entered by the user
     * @returns {Promise}
     * @memberOf thinkster.authentication.services.Authentication
     */
    function login(event, password, email) {
      var deferred = $q.defer()
      if (!email) {
        console.log('sin mail');
        $http.post(apiURL + '/login-event/', {
          event: event, password: password
        }).then(loginSuccessFn, loginErrorFn)
      }
      else{
        console.log('con mail');
        console.log(email);
         $http.post(apiURL + '/login-event/', {
          event: event, password: password, email: email
        }).then(loginSuccessFn, loginErrorFn)
      }
      /**
       * @name loginSuccessFn
       * @desc Set the authenticated account and redirect to index
       */
      function loginSuccessFn(data, status, headers, config, $q) {
        Authentication.setAuthenticatedAccount(data.data);
        Authentication.setLanguages('es');
        window.location = '/#/';
        deferred.resolve(data, status);
      }

      /**
       * @name loginErrorFn
       * @desc Log "Epic failure!" to the console
       */
      function loginErrorFn(data, status, headers, config) {
          deferred.reject(data, status, headers, config);
      }
      return deferred.promise;
    }

    function getAuthenticatedAccount() {
      // if (!$cookies.authenticatedAccount) {
      //   console.log('if !$cookies.authenticatedAccount');
      //   return;
      // }
      if (!$cookies.getObject('id_event')) {
        return;
      }
      
      return $cookies.getObject('id_event');
    }
        /**
     * @name isAuthenticated
     * @desc Check if the current user is authenticated
     * @returns {boolean} True is user is authenticated, else false.
     * @memberOf thinkster.authentication.services.Authentication
     */
    function isAuthenticated() {

      return !!$cookies.getObject('id_event');
      console.log($window.localStorage.getItem('id_event'));
    }
        /**
     * @name setAuthenticatedAccount
     * @desc Stringify the account object and store it in a cookie
     * @param {Object} user The account object to be stored
     * @returns {undefined}
     * @memberOf thinkster.authentication.services.Authentication
     */
    function setAuthenticatedAccount(account) {
      $cookies.putObject('id_event', account);
      $window.localStorage.setItem('id_event', account);
      // $cookies.authenticatedAccount = JSON.stringify(account);
    }
        /**
     * @name unauthenticate
     * @desc Delete the cookie where the user object is stored
     * @returns {undefined}
     * @memberOf thinkster.authentication.services.Authentication
     */
    function unauthenticate() {
      // delete $cookies.authenticatedAccount;
      $cookies.remove('id_event');
      $location.path('/login/');
      localStorage.removeItem('id_event');
      
    }
  /**
 * @name logout
 * @desc Try to log the user out
 * @returns {Promise}
 * @memberOf thinkster.authentication.services.Authentication
 */
function logout() {
  // return $http.post(apiURL + 'auth/logout/')
    // .then(logoutSuccessFn, logoutErrorFn);
    $cookies.remove('id_event');
    return 

  /**
   * @name logoutSuccessFn
   * @desc Unauthenticate and redirect to index with page reload
   */
  function logoutSuccessFn(data, status, headers, config) {
    Authentication.unauthenticate();
    // window.location = '/login';
  }

  /**
   * @name logoutErrorFn
   * @desc Log "Epic failure!" to the console
   */
  function logoutErrorFn(data, status, headers, config) {
    console.error('Epic failure!');
  }

}

  function getLanguages(){
        var language = $cookies.getObject('idioma');
        if (!language) {
          language = [];
        } else {
          // language = JSON.parse(language);
        }
        return language;
    }

  function setLanguages(selected_language){
      $cookies.remove('idioma');
      $cookies.putObject('idioma', selected_language );
      window.location = '/#/';
    }


}
})();
(function () {
  'use strict';

  angular
    .module('jsav.home', [
      'jsav.home.controllers',
      'jsav.home.services'
    ]);

  angular
    .module('jsav.home.controllers', []);

  angular
    .module('jsav.home.services', []);
})();

/**
* LoginController
* @namespace jsav.authentication.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.home.controllers')
    .controller('HomeController', HomeController)


HomeController.$inject = ['$cookies', '$location', '$stateParams','Authentication', 'JsavRestangular','$window', '$rootScope','$scope','$translate',];

function HomeController($cookies,$location, $stateParams, Authentication, JsavRestangular,$window, $rootScope, $scope, $translate){
	var vm = this;
	vm.title = "Inicio" ;
	vm.isAuthenticated = Authentication.isAuthenticated();
	vm.authenticatedAccount = Authentication.getAuthenticatedAccount();
	vm.language = {};

	vm.getLanguages = getLanguages;
	vm.setLanguages = setLanguages;

    activate();
	/**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf jsav.authentication.controllers.LoginController
    */
	    function activate() {
			var event = Authentication.getAuthenticatedAccount();
			vm.event_id = event.pk;
			$scope.toolbar_color = event.toolbar_color;
			$rootScope.toolbar_color = $scope.toolbar_color;
			$scope.menu_color = event.menu_color;
			$rootScope.menu_color = $scope.menu_color;
			$scope.title_color = event.title_color;
			$rootScope.title_color = $scope.title_color; 
			$rootScope.event = event;
            
	    	if (!event) {
	        	$location.path('/login');

	    	}else{
	    		if ($window.localStorage.getItem('idioma_enabled') == 'false' ) {
	    			console.log('si idioma_enabled es false');
	    			$translate.use(event.idiomas);
	    			$window.localStorage.setItem('idioma_enabled','true');
	    		}
	    		
	    	}

		}
		function getLanguages(){
        	Authentication.getLanguages();
		}

		function setLanguages(selected_language){
			Authentication.setLanguages(selected_language);
		}

	}

})()
/**
* LoginController
* @namespace jsav.authentication.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav')
    .controller('Footer', Footer)

Footer.$inject = ['$scope', 'Authentication'];

function Footer($scope, Authentication){
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();
    // vm.id = $stateParams;
    vm.event = Authentication.getAuthenticatedAccount();
    activate();
    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf jsav.authentication.controllers.LoginController
    */
        function activate() {
            if (!vm.event) {

            }
        }
        // if (vm.isAuthenticated) {
        //         vm.logo_hotel = vm.event.logo_hotel.split('/');
        //         if (vm.logo_hotel.length > 5 ) {
        //             vm.logo_footer = true;
        //             console.log(vm.logo_footer);
        //         }
        //         else{
        //             vm.logo_footer = false;   
        //             console.log(vm.logo_footer);
        //         }
            
        // }

    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.facilities', [
      'jsav.facilities.controllers',
      'jsav.facilities.services'

    ]);

  angular
    .module('jsav.facilities.controllers', ['uiGmapgoogle-maps', 'ngResource']);

  angular
    .module('jsav.facilities.services', []);
})();
/**
* LoginController
* @namespace jsav.facilities.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.facilities.controllers', ['uiGmapgoogle-maps'])
    .controller('FacilitiesController', FacilitiesController);
  	

  	FacilitiesController.$inject = ['$cookies', 'Authentication', 'JsavRestangular', '$stateParams','uiGmapGoogleMapApi', 'facilities', '$scope', '$rootScope'];

    function FacilitiesController ($cookies, Authentication, JsavRestangular, $stateParams, uiGmapGoogleMapApi, facilities, $scope, $rootScope){
    	var vm = this;
    	vm.texto = {
    		title: 'Facilidades',
    		location:'Ubicación',
    	};
    	vm.facility={};

    	activate();
		function activate() {

			if (Authentication.isAuthenticated()) {
                var event = Authentication.getAuthenticatedAccount();
                $scope.toolbar_color = event.toolbar_color;
                $rootScope.toolbar_color = $scope.toolbar_color;
                $scope.menu_color = event.menu_color;
                $rootScope.menu_color = $scope.menu_color;
                $scope.title_color = event.title_color;
                $rootScope.title_color = $scope.title_color; 
        				
                vm.facility = facilities[0];
                vm.location=vm.facility.location.split(',');
        				vm.map = {center: {latitude:vm.location[0], longitude: vm.location[1] }, zoom: 12 };
        		     	vm.options = {scrollwheel: false};
        		     	vm.marker = vm.map.center;
        		    }
		}
		 
    }

})();
/**
* Facility
* @namespace jsav.facility.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.facilities.services')
    .factory('JsavRestangular', JsavRestangular);

    JsavRestangular.$inject = ['Restangular', 'Authentication', '$http','$cookies'];
    
    function JsavRestangular(Restangular, Authentication, $http, $cookies){
      delete $http.defaults.headers.common.Authorization;
      var vm = this;
      vm.language = Authentication.getLanguages();
      return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl('http://localhost:8005/v1/');
    });

    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.conferences', [
      'jsav.conferences.controllers',
      'jsav.conferences.directives',
      'jsav.conferences.services'

    ]);

  angular
    .module('jsav.conferences.controllers', []);

   angular
    .module('jsav.conferences.directives', []);

  angular
    .module('jsav.conferences.services', []);

})();
/**
* conferences
* @namespace jsav.conferences.directives
*/
(function(){
	'use strict';


	angular
		.module('jsav.conferences.directives')
		.directive('conferences', conferences);
		/**
  		* @namespace conferences
  		*/

  	function conferences(){
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf jsav.conferences.directives.Conferences
    */  			
    var directive = {
      controller: 'ConferencesController',
      controllerAs: 'vm',
      restrict: 'E',
      scope: {
        conferences: '='
      },
      templateUrl: 'templates/conferences/conferences-directive.html'
    };    

    return directive;
    
  	}
})();
/**
* conference
* @namespace jsav.conference.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.conferences.services')
    .factory('ConferencesService', ConferencesService);

    ConferencesService.$inject = ['Restangular'];
    
    function ConferencesService(Restangular){
      
	}

})();
/**
* ConferenceController
* @namespace jsav.conference.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('ConferenceController', ConferenceController);
  	

  	ConferenceController.$inject = ['$location', 'Authentication', 'JsavRestangular','$scope','$rootScope'];

    function ConferenceController ($location, Authentication, JsavRestangular,$scope,$rootScope){
    	var vm = this;
      vm.loading = true;
    	vm.conferences=[];
    	vm.texto = {
    		title: 'Agenda',
        url: 'agenda',
    	};

    	activate();
		
		function activate() {
			if (Authentication.isAuthenticated()) {
              var event = Authentication.getAuthenticatedAccount();
              vm.event_id = event.pk;
              $scope.toolbar_color = event.toolbar_color;
              $rootScope.toolbar_color = $scope.toolbar_color;
              $scope.menu_color = event.menu_color;
              $rootScope.menu_color = $scope.menu_color;
              $scope.title_color = event.title_color;
              $rootScope.title_color = $scope.title_color;      
            //http.get('events/5/conferences/') get_lis.a
		        JsavRestangular.one('events',vm.event_id).getList('conferences').then(function(conferences) {
              vm.loading = false;
        			vm.conferences = conferences;
    			 });

		    }
		  }
        
    }

})();
/**
* DetailConferencesController
* @namespace jsav.conference.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('Detail', Detail);
  	

  	Detail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function Detail ($location, $scope, Authentication, JsavRestangular, $stateParams,$rootScope){
    	var vm = this;
      vm.loading = true;
    	vm.single_conrence=[];
      var event = Authentication.getAuthenticatedAccount();
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
  		        vm.event_id = event.pk;
  		        $stateParams.id
  		        JsavRestangular.one('events',vm.event_id).one('conferences', $stateParams.id).get().then(function (conferences) {
                vm.loading = false;
          			vm.single_conrence = conferences;
      			})
  		    }
  		}
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.assistents', [
      'jsav.assistents.controllers',
      'jsav.assistents.services'

    ]);

  angular
    .module('jsav.assistents.controllers', []);

  angular
    .module('jsav.assistents.services', []);

})();
/**
* SpeakersService
* @namespace jsav.assistents.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.assistents.services')
    .factory('assistentsService', assistentsService);

    assistentsService.$inject = ['Restangular'];
    
    function assistentsService(Restangular){
      
	}

})();
/**
* AssistentsController
* @namespace jsav.assistents.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.assistents.controllers')
    .controller('AssistentsController', AssistentsController);
  	

  	AssistentsController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope','$rootScope', '$interval', '$timeout', '$window'];

    function AssistentsController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope, $interval, $timeout, $window){
    	var vm = this, j= 0, counter = 0;
        vm.activated = true;
        vm.determinateValue = 30;
        vm.isLoading = false;
    	vm.assistents=[];
      vm.goToassistent = goToassistent;
      vm.doSendEmail = doSendEmail;


        vm.showList = [ ];
    $interval(function() {
      vm.determinateValue += 4;

      if (vm.determinateValue > 100) vm.determinateValue = 30;

        // Incrementally start animation the five (5) Indeterminate,
        // themed progress circular bars

        if ( (j < 2) && !vm.showList[j] && vm.activated ) {
          vm.showList[j] = true;
        }
        if ( counter++ % 4 == 0 ) j++;

        // Show the indicator in the "Used within Containers" after 200ms delay
        if ( j == 2 ) vm.contained = "indeterminate";

    }, 100, 0, true);

    $interval(function() {
      vm.mode = (vm.mode == 'query' ? 'determinate' : 'query');
    }, 7200, 0, true);
        /**
         * Turn off or on the 5 themed loaders
         */
        vm.toggleActivation = function() {
            if ( !vm.activated ) vm.showList = [ ];
            if (  vm.activated ) {
              j = counter = 0;
              vm.determinateValue = 30;
              vm.determinateValue2 = 30;
            }
        };
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color;
            $scope.color_font_toolbar = event.color_font_toolbar;
            $rootScope.color_font_toolbar = $scope.color_font_toolbar;
             $timeout(function(){ 
                JsavRestangular.one('events',vm.event_id).all('assistents').getList().then(function (assistents) {
                    vm.assistents = assistents;
                    vm.determinateValue = 100;
                    vm.isLoading = true;

                });
            },1500);

		    }


        
		  }
      function goToassistent(assistent){
          $location.path('/asistentes/'+assistent+"/");
      }
      function doSendEmail(email){
          $window.location = "mailto:" + email;
      }


		
    }

})();
/**
* AssistentDetail
* @namespace jsav.assistents.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.assistents.controllers')
    .controller('AssistentDetail', AssistentDetail);
    

    AssistentDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function AssistentDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;
      vm.single_assistent={};

      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {

            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

              JsavRestangular.one('events',vm.event_id).one('assistents', $stateParams.id).get().then(function (assistent) {
                vm.loading = false;
                vm.single_assistent = assistent;
            })
        }
        else{
          $location.path('/login');
        }
      }
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.speakers', [
      'jsav.speakers.controllers',
      'jsav.speakers.services'

    ]);

  angular
    .module('jsav.speakers.controllers', []);

  angular
    .module('jsav.speakers.services', []);

})();
/**
* SpeakersService
* @namespace jsav.speakers.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.speakers.services')
    .factory('speakersService', speakersService);

    speakersService.$inject = ['Restangular'];
    
    function speakersService(Restangular){
      
	}

})();
/**
* SpeakersController
* @namespace jsav.speakers.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.speakers.controllers')
    .controller('SpeakersController', SpeakersController);
  	

  	SpeakersController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function SpeakersController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.speakers=[];
    	vm.texto = {
    		title: 'Conferencistas',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

            JsavRestangular.one('events',vm.event_id).all('speakers').getList().then(function (speakers) {
                    vm.loading = false;
        			vm.speakers = speakers;
    			});
		    }

        
		  }
		
    }

})();
/**
* SpeakerDetail
* @namespace jsav.speakers.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.speakers.controllers')
    .controller('SpeakerDetail', SpeakerDetail);
    

    SpeakerDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function SpeakerDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;
      vm.single_speaker={};


      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
              JsavRestangular.one('events',vm.event_id).one('speakers', $stateParams.id).get().then(function (speakers) {
                vm.loading = false;
                vm.single_speaker = speakers;
                vm.texto = {
                   title:vm.single_speaker.name,
                   url: 'agenda',
              };
            })
          }
      }
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.links', [
      'jsav.links.controllers',
      'jsav.links.services'

    ]);

  angular
    .module('jsav.links.controllers', []);

  angular
    .module('jsav.links.services', []);

})();
/**
* LinkController
* @namespace jsav.conference.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.links.controllers')
    .controller('LinkController', LinkController);
  	

  	LinkController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams','$scope','$rootScope'];

    function LinkController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.conferences=[];
    	vm.texto = {
    		title: 'Links',
        url: 'links',
    	};

    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

		        JsavRestangular.one('events',vm.event_id).all('conferences').getList().then(function (conferences) {
                    vm.loading = false;
        			vm.conferences = conferences;
    			});
			
		    }
		  }
		
    }

})();
/**
* LinkDetail
* @namespace jsav.links.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('LinkDetail', LinkDetail);
  	

  	LinkDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function LinkDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
    	var vm = this;
        vm.loading = true;        
    	vm.single_conrence=[];


    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
            
		        $stateParams.id
		        // console.log($stateParams);
		        JsavRestangular.one('events',vm.event_id).one('conferences', $stateParams.id).get().then(function (conferences) {
                    vm.loading = false;
        			vm.single_conrence = conferences;
        			vm.texto = {
    					title:'Links',
    				};
    			})
		    }
		}
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.downloads', [
      'jsav.downloads.controllers',
      'jsav.downloads.services'

    ]);

  angular
    .module('jsav.downloads.controllers', []);

  angular
    .module('jsav.downloads.services', []);

})();
/**
* DownloadsService
* @namespace jsav.downloads.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.downloads.services')
    .factory('DownloadsService', DownloadsService);

    DownloadsService.$inject = ['Restangular'];
    
    function DownloadsService(Restangular){
      
	}

})();
/**
* /**
* DownloadController
* @namespace jsav.downloads.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.downloads.controllers')
    .controller('DownloadController', DownloadController);
    

    DownloadController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function DownloadController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
      var vm = this;
      vm.loading = true;    
      vm.conferences=[];
      vm.texto = {
        title: 'Descargas',
        url: 'descargas',
      };
      // var event = Authentication.getAuthenticatedAccount();
      
      activate();
    
    function activate() {

       if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
              
            JsavRestangular.one('events',vm.event_id).all('conferences').getList().then(function (conferences) {
              vm.loading = false;    
              vm.conferences = conferences;
          });
      
        }
      }
    
    }

})();
/**
* DownloadDetail
* @namespace jsav.download.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('DownloadDetail', DownloadDetail);
    

    DownloadDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function DownloadDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;    
      vm.single_conrence=[];


      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

            $stateParams.id
            // console.log($stateParams);
            JsavRestangular.one('events',vm.event_id).one('conferences', $stateParams.id).get().then(function (conferences) {
              vm.loading = false;    
              vm.single_conrence = conferences;
              vm.texto = {
              title:vm.single_conrence.name,
              conferencistas: 'Descargas',
              links: 'Descargas',
              descargas: 'Descargas',
            };
          })
        }
    }
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.newsletters', [
      'jsav.newsletters.controllers',
      'jsav.newsletters.services'

    ]);

  angular
    .module('jsav.newsletters.controllers', []);

  angular
    .module('jsav.newsletters.services', []);

})();
/**
* conference
* @namespace jsav.conference.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.conferences.services')
    .factory('ConferencesService', ConferencesService);

    ConferencesService.$inject = ['Restangular'];
    
    function ConferencesService(Restangular){
      
	}

})();
/**
* NewslettersController
* @namespace jsav.newsletters.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.newsletters.controllers')
    .controller('NewslettersController', NewslettersController);
  	

  	NewslettersController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function NewslettersController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.conferences=[];
    	vm.texto = {
    		title: 'Newsletter',
        url: 'boletines',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

	        JsavRestangular.one('events',vm.event_id).all('newsletters').getList().then(function (conferences) {
                vm.loading = false;
    			vm.conferences = conferences;
			});
		    }
        
		  }
		
    }

})();
/**
* NewsletterDetailConferencesController
* @namespace jsav.conference.NewsletterController.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.newsletters.controllers')
    .controller('NewsletterDetail', NewsletterDetail);
  	

  	NewsletterDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function NewsletterDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.single_newsletter =[];


    	activate();
		
		function activate() {

		  if (Authentication.isAuthenticated()) {
			var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
	        $stateParams.id
	        // console.log($stateParams);
	        JsavRestangular.one('events',vm.event_id).one('newsletters', $stateParams.id).get().then(function (newsletter) {
                vm.loading = false;
    			vm.single_newsletter = newsletter;
    			vm.texto = {
                    title: vm.single_newsletter.name,
				};
			})
	       }
		}
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.tips', [
      'jsav.tips.controllers',
      'jsav.tips.directives',
      'jsav.tips.services'

    ]);

  angular
    .module('jsav.tips.controllers', []);

   angular
    .module('jsav.tips.directives', []);

  angular
    .module('jsav.tips.services', []);

})();
/**
* TipsService
* @namespace jsav.tips.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.tips.services')
    .factory('TipsService', TipsService);

    TipsService.$inject = ['Restangular'];
    
    function TipsService(Restangular){
      
	}
})();
/**
* TipsController
* @namespace jsav.conference.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.tips.controllers')
    .controller('TipsController', TipsController);
  	

  	TipsController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function TipsController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.conferences=[];
    	vm.texto = {
    		title: 'Tips',
        url: 'tips',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

		        JsavRestangular.one('events',vm.event_id).all('tips').getList().then(function (tips) {
                    vm.loading = false;
        			vm.conferences = tips;
    			});

		    }
		  }
		
    }

})();
/**
* TipDetailController
* @namespace jsav.tips.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.tips.controllers')
    .controller('TipDetailController', TipDetailController);
  	

  	TipDetailController.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function TipDetailController ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
    	var vm = this;
    	vm.single_tip=[];


    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

		        JsavRestangular.one('events',vm.event_id).one('tips', $stateParams.id).get().then(function (tips) {
        			vm.single_tip = tips;
        			vm.texto = {
    					   title:vm.single_tip.name,
    				};
    			})
		    }
		}
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.sponsors', [
      'jsav.sponsors.controllers',
      'jsav.sponsors.services'

    ]);

  angular
    .module('jsav.sponsors.controllers', []);

  angular
    .module('jsav.sponsors.services', []);

})();
/**
* sponsorsService
* @namespace jsav.sponsors.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.sponsors.services')
    .factory('sponsorsService', sponsorsService);

    sponsorsService.$inject = ['Restangular'];
    
    function sponsorsService(Restangular){
      
	}

})();
/**
* SponsorsController
* @namespace jsav.speakers.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.sponsors.controllers')
    .controller('SponsorsController', SponsorsController);
  	

  	SponsorsController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function SponsorsController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.sponsors={};
    	vm.texto = {
    		title: 'Patrocinadores',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color;
             
		        JsavRestangular.one('events',vm.event_id).one('sponsors').get().then(function (sponsors) {
                    vm.loading = false;
        			vm.sponsors = sponsors;
    			});
		    }

        
		  }
		
    }

})();
/**
* SponsorDetailController
* @namespace jsav.sponsors.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.sponsors.controllers')
    .controller('SponsorDetailController', SponsorDetailController);
    

    SponsorDetailController.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function SponsorDetailController ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;
      vm.single_sponsors={};


      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
              JsavRestangular.one('events',vm.event_id).one('sponsors', $stateParams.id).get().then(function (sponsors) {
                vm.loading = false;
                vm.single_sponsors = sponsors;
                vm.texto = {
                   title:vm.single_sponsors.name,
                   url: 'conferencias',
              };
            })
          }
      }
    }

})();
(function () {
  'use strict';

  angular
    .module('jsav.auth', [
      'jsav.auth.controllers',
      'jsav.auth.services'
    ]);

  angular
    .module('jsav.auth.controllers', ['ngMessages']);

  angular
    .module('jsav.auth.services', ['ngCookies']);
})();
'use strict';

/**
 * @ngdoc function
 * @name angularDjangoRegistrationAuthApp.controller:AuthrequiredCtrl
 * @description
 * # AuthrequiredCtrl
 * Controller of the angularDjangoRegistrationAuthApp
 */
angular.module('jsav.auth.controllers')
  .controller('AuthrequiredCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      // 'AngularJS',
      // 'Karma'
    ];
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('LoginCtrl', function ($scope, $rootScope,$location, djangoAuth, Validate, Authentication, authenticated) {
    var vm = this;
  	vm.complete = false;
    vm.login = login;
    vm.authenticated = false;
    vm.greeting = authenticated;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
        $location.path('userProfile');
    });

    if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
    };
    function login (formData){
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
      if(!formData.$invalid){
        djangoAuth.login(vm.username, vm.password)
        .then(function(data){
        	// success case
        	// $location.path("/ajustes");
          vm.complete = true;
        },function(data){
        	// error case
        	vm.errors = data;
        });
      }
    }
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('LogoutCtrl', function ($scope, $location, djangoAuth) {
    djangoAuth.logout();
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('MainCtrl', function ($scope, $cookies, $location, djangoAuth) {
    
    $scope.login = function(){
      djangoAuth.login(prompt('Username'),prompt('password'))
      .then(function(data){
        handleSuccess(data);
      },handleError);
    }
    
    $scope.logout = function(){
      djangoAuth.logout()
      .then(handleSuccess,handleError);
    }
    
    $scope.resetPassword = function(){
      djangoAuth.resetPassword(prompt('Email'))
      .then(handleSuccess,handleError);
    }
    
    $scope.register = function(){
      djangoAuth.register(prompt('Username'),prompt('Password'),prompt('Email'))
      .then(handleSuccess,handleError);
    }
    
    $scope.verify = function(){
      djangoAuth.verify(prompt("Please enter verification code"))
      .then(handleSuccess,handleError);
    }
    
    $scope.goVerify = function(){
      $location.path("/verifyEmail/"+prompt("Please enter verification code"));
    }
    
    $scope.changePassword = function(){
      djangoAuth.changePassword(prompt("Password"), prompt("Repeat Password"))
      .then(handleSuccess,handleError);
    }
    
    $scope.profile = function(){
      djangoAuth.profile()
      .then(handleSuccess,handleError);
    }
    
    $scope.updateProfile = function(){
      djangoAuth.updateProfile({'first_name': prompt("First Name"), 'last_name': prompt("Last Name"), 'email': prompt("Email")})
      .then(handleSuccess,handleError);
    }
    
    $scope.confirmReset = function(){
      djangoAuth.confirmReset(prompt("Code 1"), prompt("Code 2"), prompt("Password"), prompt("Repeat Password"))
      .then(handleSuccess,handleError);
    }

    $scope.goConfirmReset = function(){
      $location.path("/passwordResetConfirm/"+prompt("Code 1")+"/"+prompt("Code 2"))
    }
    
    var handleSuccess = function(data){
      $scope.response = data;
    }
    
    var handleError = function(data){
      $scope.response = data;
    }

    $scope.show_login = true;
    $scope.$on("djangoAuth.logged_in", function(data){
      $scope.show_login = false;
    });
    $scope.$on("djangoAuth.logged_out", function(data){
      $scope.show_login = true;
    });

  });

'use strict';

angular.module('jsav.auth.controllers')
  .factory('MasterCtrl', function ($scope, $location, djangoAuth) {
    // Assume user is not logged in until we hear otherwise
    $scope.authenticated = false;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        $scope.authenticated = true;
    });
    // Wait and respond to the logout event.
    $scope.$on('djangoAuth.logged_out', function() {
      $scope.authenticated = false;
    });
    // Wait and respond to the log in event.
    $scope.$on('djangoAuth.logged_in', function() {
      $scope.authenticated = true;
    });
    // If the user attempts to access a restricted page, redirect them back to the main page.
    $scope.$on('$routeChangeError', function(ev, current, previous, rejection){
      console.error("Unable to change routes.  Error: ", rejection)
      $location.path('/restricted').replace();
    });
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('PasswordchangeCtrl', function ($scope,$rootScope,Authentication, djangoAuth, Validate) {
    var vm = this;
    vm.model = {'new_password1':'','new_password2':''};
    vm.complete = false;
    vm.authenticated = false;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
    });
    var event = Authentication.getAuthenticatedAccount();
    $scope.toolbar_color = event.toolbar_color;
    $rootScope.toolbar_color = $scope.toolbar_color;
    $scope.menu_color = event.menu_color;
    $rootScope.menu_color = $scope.menu_color;
    $scope.title_color = event.title_color;
    $rootScope.title_color = $scope.title_color; 

    vm.changePassword = function(formData){
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
      if(!formData.$invalid){
        djangoAuth.changePassword(vm.model.new_password1, vm.model.new_password2)
        .then(function(data){
        	// success case
        	vm.complete = true;
        },function(data){
        	// error case
        	vm.errors = data;
        });
      }
    }
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('PasswordresetCtrl', function ($scope,$rootScope, Authentication, djangoAuth, Validate) {
    $scope.model = {'email':''};
  	$scope.complete = false;

    var event = Authentication.getAuthenticatedAccount();
    $scope.toolbar_color = event.toolbar_color;
    $rootScope.toolbar_color = $scope.toolbar_color;
    $scope.menu_color = event.menu_color;
    $rootScope.menu_color = $scope.menu_color;
    $scope.title_color = event.title_color;
    $rootScope.title_color = $scope.title_color; 
    $scope.resetPassword = function(formData){
      $scope.errors = [];
      Validate.form_validation(formData,$scope.errors);
      if(!formData.$invalid){
        djangoAuth.resetPassword($scope.model.email)
        .then(function(data){
        	// success case
        	$scope.complete = true;
        },function(data){
        	// error case
        	$scope.errors = data;
        });
      }
    }
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('PasswordresetconfirmCtrl', function ($scope, $routeParams, djangoAuth, Validate) {
    $scope.model = {'new_password1':'','new_password2':''};
  	$scope.complete = false;
    $scope.confirmReset = function(formData){
      $scope.errors = [];
      Validate.form_validation(formData,$scope.errors);
      if(!formData.$invalid){
        djangoAuth.confirmReset($routeParams['firstToken'], $routeParams['passwordResetToken'], $scope.model.new_password1, $scope.model.new_password2)
        .then(function(data){
        	// success case
        	$scope.complete = true;
        },function(data){
        	// error case
        	$scope.errors = data;
        });
      }
    }
  });

(function(){
  'use strict';

angular.module('jsav.auth.controllers')
  .controller('RegisterCtrl', function ($scope, $location, djangoAuth, Validate, $cookies,Authentication) {
    var vm = this;

  	// vm.model = {'username':'','password':'','email':''};

  	vm.complete = false;

    vm.authenticated = false;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
    });
    // Wait and respond to the logout event.
    $scope.$on('djangoAuth.logged_out', function() {
      vm.authenticated = false;
    });
    // Wait and respond to the log in event.
    $scope.$on('djangoAuth.logged_in', function() {
      vm.authenticated = true;
    });
    
    vm.register = function(formData){
      
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
    
      if(!formData.$invalid){
        var event = Authentication.getAuthenticatedAccount();
        vm.model.event = event.pk;
        djangoAuth.register( 
          vm.model.username,
          vm.model.password,
          vm.model.email,
          vm.model.fist_name,
          vm.model.last_name,
          vm.model.event,  
          vm.model.show_assistent,
          vm.model.company,
          vm.model.job,
          vm.model.biography
        )
        .then(function(data){
        	// success case
        	vm.complete = true;
          $location.path("#/ajustes/");
          
        },function(data){
        	// error case
        	vm.errors = data;
        });
      }
    }
  });

})();
'use strict';

/**
 * @ngdoc function
 * @name angularDjangoRegistrationAuthApp.controller:RestrictedCtrl
 * @description
 * # RestrictedCtrl
 * Controller of the angularDjangoRegistrationAuthApp
 */
angular.module('jsav.auth.controllers')
  .controller('RestrictedCtrl', function ($scope, $location) {
    $scope.$on('djangoAuth.logged_in', function() {
      $location.path('/');
    });
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('UserprofileCtrl', function ($scope,$rootScope, djangoAuth, Validate, Authentication, $cookies, $window) {
    
    var vm = this;
    vm.complete = false;    
    vm.authenticated = false;

    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
        // vm.model = {'username':'', 'first_name':'','last_name':'','email':'','event':'','company':'', 'job':'', 'biography':''};

        var event = Authentication.getAuthenticatedAccount();
        vm.event_id = event.pk;
        $scope.toolbar_color = event.toolbar_color;
        $rootScope.toolbar_color = $scope.toolbar_color;
        $scope.menu_color = event.menu_color;
        $rootScope.menu_color = $scope.menu_color;
        $scope.title_color = event.title_color;
        $rootScope.title_color = $scope.title_color; 

        djangoAuth.profile().then(function(data){
            vm.model = data;
            console.log(data);
            $cookies.putObject('assistent_id',data.assistent_id);

        });
    });

     // Wait and respond to the logout event.
    $scope.$on('djangoAuth.logged_out', function() {
      vm.authenticated = false;
    });
    // Wait and respond to the log in event.
    $scope.$on('djangoAuth.logged_in', function() {
      vm.authenticated = true;
    });
    
    vm.changeComplete = function(param){
      vm.complete = param;
    }

    vm.updateProfile = function(formData, model){
      vm.errors = [];
      Validate.form_validation(formData, vm.errors);
      if(!formData.$invalid){
        djangoAuth.updateProfile(model)
        .then(function(data){
        	// success case
        	vm.complete = false;
        },function(data){
        	// error case
          console.log("error case");
          console.log(data);
        	vm.error = data;
        });
      }
    }
  });

'use strict';

angular.module('jsav.auth.controllers')
  .controller('VerifyemailCtrl', function ($scope, $routeParams, djangoAuth) {
    djangoAuth.verify($routeParams["emailVerificationToken"]).then(function(data){
    	$scope.success = true;
    },function(data){
    	$scope.failure = false;
    });
  });

'use strict';

angular.module('jsav.auth.services')
  .service('djangoAuth', function djangoAuth($q, $http, $cookies, $rootScope, Authentication, $location) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    if (!Authentication.isAuthenticated()) {
        $location.path('/login');
    }
    else{
        var event = Authentication.getAuthenticatedAccount();
        var id = event.pk;
    }
    
    console.log(id);
    var API_URL = "//localhost:8005/rest-auth";
    var API_URL_ASSISTENT = "//localhost:8005/v1";
    var service = {
        /* START CUSTOMIZATION HERE */
        // Change this to point to your Django REST Auth API
        // e.g. /api/rest-auth  (DO NOT INCLUDE ENDING SLASH)

        'API_URL': '//localhost:8005/rest-auth',
        'API_URL_ASSISTENT':'//localhost:8005/v1',


        // Set use_session to true to use Django sessions to store security token.
        // Set use_session to false to store the security token locally and transmit it as a custom header.
        'use_session': true,
        /* END OF CUSTOMIZATION */
        'authenticated': null,
        'authPromise': null,
        'authenticated_event':null,

        'request': function(args, api_url) {
            // Let's retrieve the token from the cookie, if available
            if($cookies.getObject('token')){
                $http.defaults.headers.common.Authorization = 'Token ' + $cookies.getObject('token');
            }
            
            // Continue
            params = args.params || {}
            args = args || {};
            var deferred = $q.defer(),
                url = api_url + args.url,
                method = args.method || "GET",
                params = params,
                data = args.data || {};
            // Fire the request, as configured.
            $http({
                url: url,
                withCredentials: this.use_session,
                method: method.toUpperCase(),
                headers: {
                        'X-CSRFToken': $cookies['csrftoken'],
                        
                },
                params: params,
                data: data
            })
            .success(angular.bind(this,function(data, status, headers, config) {
                deferred.resolve(data, status);
            }))
            .error(angular.bind(this,function(data, status, headers, config) {
                console.log("error syncing with: " + url);
                // Set request status
                if(data){
                    data.status = status;
                }
                if(status == 0){
                    if(data == ""){
                        data = {};
                        data['status'] = 0;
                        data['non_field_errors'] = ["Could not connect. Please try again."];
                    }
                    // or if the data is null, then there was a timeout.
                    if(data == null){
                        // Inject a non field error alerting the user
                        // that there's been a timeout error.
                        data = {};
                        data['status'] = 0;
                        data['non_field_errors'] = ["Server timed out. Please try again."];
                    }
                }
                deferred.reject(data, status, headers, config);
            }));
            return deferred.promise;
        },
        'register': function(username,password,email,first_name, last_name, event, show_assistent, company,job, biography, more){
            var data = {
                'username':username,
                'password':password,
                'email':email,
                'first_name': first_name,
                'last_name': last_name,
                'event': event,
                'show_assistent': show_assistent,
                'company': company,
                'job': job,
                'biography': biography

            }
            // data = angular.extend(data,more);
            var event = Authentication.getAuthenticatedAccount();
            var id = event.pk;
            return this.request({
                'method': "POST",
                'url': "/events/"+ id +"/assistents/",
                'data' :data
            },API_URL_ASSISTENT);
        },
        'login': function(username,password){
            var djangoAuth = this;
            return this.request({
                'method': "POST",
                'url': "/login/",
                'data':{
                    'username':username,
                    'password':password
                }
            },this.API_URL).then(function(data){
                if(!djangoAuth.use_session){
                    $http.defaults.headers.common.Authorization = 'Token ' + data.key;
                    $cookies.putObject('token', data.key);
                }
                djangoAuth.authenticated = true;

                $rootScope.$broadcast("djangoAuth.logged_in", data);
                $location.path('/userProfile');
            });
        },
        'logout': function(){
            var djangoAuth = this;
            return this.request({
                'method': "POST",
                'url': "/logout/"
            },this.API_URL).then(function(data){
                delete $http.defaults.headers.common.Authorization;
                // delete $cookies.token;
                $cookies.remove('token');
                djangoAuth.authenticated = false;
                $rootScope.$broadcast("djangoAuth.logged_out");
                $location.path('/ajustes/');
            });
        },
        'changePassword': function(password1,password2){
            return this.request({
                'method': "POST",
                'url': "/password/change/",
                'data':{
                    'new_password1':password1,
                    'new_password2':password2
                }
            },this.API_URL);
        },
        'resetPassword': function(email){
            return this.request({
                'method': "POST",
                'url': "/password/reset/",
                'data':{
                    'email':email
                }
            }, this.API_URL);
        },
        'profile': function(){
            return this.request({
                'method': "GET",
                'url': "/events/"+id+"/assistents/?detail=true",
            }, API_URL_ASSISTENT); 
        },
        'updateProfile': function(data){
            console.log("updateProfile");
            console.log(data);
            var event = Authentication.getAuthenticatedAccount();
            var id = event.pk;
            return this.request({
                'method': "PUT",
                'url': "/events/"+id+"/assistents/"+ $cookies.getObject('assistent_id') +"/",
                'data':data
            }, this.API_URL_ASSISTENT); 
        },
        'verify': function(key){
            return this.request({
                'method': "POST",
                'url': "/registration/verify-email/",
                'data': {'key': key} 
            }, this.API_URL);            
        },
        'confirmReset': function(uid,token,password1,password2){
            return this.request({
                'method': "POST",
                'url': "/password/reset/confirm/",
                'data':{
                    'uid': uid,
                    'token': token,
                    'new_password1':password1,
                    'new_password2':password2
                }
            }, this.API_URL);
        },
        'authenticationStatus': function(restrict, force){
            djangoAuth.authenticated_event = Authentication.isAuthenticated();
            // Set restrict to true to reject the promise if not logged in
            // Set to false or omit to resolve when status is known
            // Set force to true to ignore stored value and query API
            restrict = restrict || false;
            force = force || false;
            if(this.authPromise == null || force){
                this.authPromise = this.request({
                    'method': "GET",
                    'url': "/user/"
                },this.API_URL)

            }
            var da = this;
            var getAuthStatus = $q.defer();
            if(this.authenticated != null && !force){
                // We have a stored value which means we can pass it back right away.
                if(this.authenticated == false && restrict){
                    getAuthStatus.reject("User is not logged in.");
                    if (!djangoAuth.authenticated_event) {
                        $location.path('/');
                    }
                    
                }else{
                    getAuthStatus.resolve();
                }
            }else{
                // There isn't a stored value, or we're forcing a request back to
                // the API to get the authentication status.
                this.authPromise.then(function(){
                    da.authenticated = true;
                    getAuthStatus.resolve();
                },function(){
                    da.authenticated = false;
                    if(restrict){
                        getAuthStatus.reject("User is not logged in.");
                        if (!djangoAuth.authenticated_event) {
                            $location.path('/login');
                        }
                    }else{
                        getAuthStatus.resolve();
                    }
                });
            }
            return getAuthStatus.promise;
        },
        'initialize': function(url, sessions){
            this.API_URL = url;
            this.use_session = sessions;
            return this.authenticationStatus();
        },
        'delete_token':function(){

            delete $http.defaults.headers.common.Authorization;
            if ($cookies.getObject('NG_TRANSLATE_LANG_KEY')) {
                var language = $cookies.getObject('NG_TRANSLATE_LANG_KEY');
                $http.defaults.headers.common['Accept-Language'] = language;
            }
            

        },
        'add_token':function(){
            if($cookies.getObject('token')){
                $http.defaults.headers.common.Authorization = 'Token ' + $cookies.getObject('token');
            }
        }

    }
    return service;
  });

'use strict';

angular.module('jsav.auth.services')
  .service('Validate', function Validate() {
    return {
        'message': {
            'minlength': 'This value is not long enough.',
            'maxlength': 'This value is too long.',
            'email': 'A properly formatted email address is required.',
            'required': 'This field is required.'
        },
        'more_messages': {
            'demo': {
                'required': 'Here is a sample alternative required message.'
            }
        },
        'check_more_messages': function(name,error){
            return (this.more_messages[name] || [])[error] || null;
        },
        validation_messages: function(field,form,error_bin){
            var messages = [];
            for(var e in form[field].$error){
                if(form[field].$error[e]){
                    var special_message = this.check_more_messages(field,e);
                    if(special_message){
                        messages.push(special_message);
                    }else if(this.message[e]){
                        messages.push(this.message[e]);
                    }else{
                        messages.push("Error: " + e)
                    }
                }
            }
            var deduped_messages = [];
            angular.forEach(messages, function(el, i){
                if(deduped_messages.indexOf(el) === -1) deduped_messages.push(el);
            });
            if(error_bin){
                error_bin[field] = deduped_messages;
            }
        },
        'form_validation': function(form,error_bin){
            for(var field in form){
                if(field.substr(0,1) != "$"){
                    this.validation_messages(field,form,error_bin);
                }
            }
        }
    }
});