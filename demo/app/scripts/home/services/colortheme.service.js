/**
* Facility
* @namespace jsav.facility.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.home.services')
    .factory('ThemingService', ThemingService);

    ThemingService.$inject = ["$rootScope"];
    
    return themingProvider = {
      definePalette: definePalette,
      extendPalette: extendPalette,
      theme: registerTheme,

      setDefaultTheme: function(theme) {
        defaultTheme = theme;
      },
      alwaysWatchTheme: function(alwaysWatch) {
        alwaysWatchTheme = alwaysWatch;
      },
      reload: generateThemes, // here
      $get: ThemingService,
      _LIGHT_DEFAULT_HUES: LIGHT_DEFAULT_HUES,
      _DARK_DEFAULT_HUES: DARK_DEFAULT_HUES,
      _PALETTES: PALETTES,
      _THEMES: THEMES,
      _parseRules: parseRules,
      _rgba: rgba
    };

})();