(function () {
  'use strict';

  angular
    .module('jsav.home', [
      'jsav.home.controllers',
      'jsav.home.services'
    ]);

  angular
    .module('jsav.home.controllers', []);

  angular
    .module('jsav.home.services', []);
})();