
/**
* LoginController
* @namespace jsav.authentication.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.home.controllers')
    .controller('HomeController', HomeController)


HomeController.$inject = ['$cookies', '$location', '$stateParams','Authentication', 'JsavRestangular','$window', '$rootScope','$scope','$translate',];

function HomeController($cookies,$location, $stateParams, Authentication, JsavRestangular,$window, $rootScope, $scope, $translate){
	var vm = this;
	vm.title = "Inicio" ;
	vm.isAuthenticated = Authentication.isAuthenticated();
	vm.authenticatedAccount = Authentication.getAuthenticatedAccount();
	vm.language = {};

	vm.getLanguages = getLanguages;
	vm.setLanguages = setLanguages;

    activate();
	/**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf jsav.authentication.controllers.LoginController
    */
	    function activate() {
			var event = Authentication.getAuthenticatedAccount();
			vm.event_id = event.pk;
			$scope.toolbar_color = event.toolbar_color;
			$rootScope.toolbar_color = $scope.toolbar_color;
			$scope.menu_color = event.menu_color;
			$rootScope.menu_color = $scope.menu_color;
			$scope.title_color = event.title_color;
			$rootScope.title_color = $scope.title_color; 
			$rootScope.event = event;
            
	    	if (!event) {
	        	$location.path('/login');

	    	}else{
	    		if ($window.localStorage.getItem('idioma_enabled') == 'false' ) {
	    			console.log('si idioma_enabled es false');
	    			$translate.use(event.idiomas);
	    			$window.localStorage.setItem('idioma_enabled','true');
	    		}
	    		
	    	}

		}
		function getLanguages(){
        	Authentication.getLanguages();
		}

		function setLanguages(selected_language){
			Authentication.setLanguages(selected_language);
		}

	}

})()