(function () {
  'use strict';

  angular
    .module('jsav.links', [
      'jsav.links.controllers',
      'jsav.links.services'

    ]);

  angular
    .module('jsav.links.controllers', []);

  angular
    .module('jsav.links.services', []);

})();