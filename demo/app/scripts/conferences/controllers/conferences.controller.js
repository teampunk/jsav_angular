/**
* ConferenceController
* @namespace jsav.conference.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('ConferenceController', ConferenceController);
  	

  	ConferenceController.$inject = ['$location', 'Authentication', 'JsavRestangular','$scope','$rootScope'];

    function ConferenceController ($location, Authentication, JsavRestangular,$scope,$rootScope){
    	var vm = this;
      vm.loading = true;
    	vm.conferences=[];
    	vm.texto = {
    		title: 'Agenda',
        url: 'agenda',
    	};

    	activate();
		
		function activate() {
			if (Authentication.isAuthenticated()) {
              var event = Authentication.getAuthenticatedAccount();
              vm.event_id = event.pk;
              $scope.toolbar_color = event.toolbar_color;
              $rootScope.toolbar_color = $scope.toolbar_color;
              $scope.menu_color = event.menu_color;
              $rootScope.menu_color = $scope.menu_color;
              $scope.title_color = event.title_color;
              $rootScope.title_color = $scope.title_color;      
            //http.get('events/5/conferences/') get_lis.a
		        JsavRestangular.one('events',vm.event_id).getList('conferences').then(function(conferences) {
              vm.loading = false;
        			vm.conferences = conferences;
    			 });

		    }
		  }
        
    }

})();