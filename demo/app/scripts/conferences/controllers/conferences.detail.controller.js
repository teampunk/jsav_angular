/**
* DetailConferencesController
* @namespace jsav.conference.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('Detail', Detail);
  	

  	Detail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function Detail ($location, $scope, Authentication, JsavRestangular, $stateParams,$rootScope){
    	var vm = this;
      vm.loading = true;
    	vm.single_conrence=[];
      var event = Authentication.getAuthenticatedAccount();
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
  		        vm.event_id = event.pk;
  		        $stateParams.id
  		        JsavRestangular.one('events',vm.event_id).one('conferences', $stateParams.id).get().then(function (conferences) {
                vm.loading = false;
          			vm.single_conrence = conferences;
      			})
  		    }
  		}
    }

})();