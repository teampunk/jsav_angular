(function () {
  'use strict';

  angular
    .module('jsav.conferences', [
      'jsav.conferences.controllers',
      'jsav.conferences.directives',
      'jsav.conferences.services'

    ]);

  angular
    .module('jsav.conferences.controllers', []);

   angular
    .module('jsav.conferences.directives', []);

  angular
    .module('jsav.conferences.services', []);

})();