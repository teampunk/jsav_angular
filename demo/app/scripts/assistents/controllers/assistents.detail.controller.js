/**
* AssistentDetail
* @namespace jsav.assistents.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.assistents.controllers')
    .controller('AssistentDetail', AssistentDetail);
    

    AssistentDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function AssistentDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;
      vm.single_assistent={};

      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {

            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

              JsavRestangular.one('events',vm.event_id).one('assistents', $stateParams.id).get().then(function (assistent) {
                vm.loading = false;
                vm.single_assistent = assistent;
            })
        }
        else{
          $location.path('/login');
        }
      }
    }

})();