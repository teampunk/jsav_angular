/**
* AssistentsController
* @namespace jsav.assistents.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.assistents.controllers')
    .controller('AssistentsController', AssistentsController);
  	

  	AssistentsController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope','$rootScope', '$interval', '$timeout', '$window'];

    function AssistentsController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope, $interval, $timeout, $window){
    	var vm = this, j= 0, counter = 0;
        vm.activated = true;
        vm.determinateValue = 30;
        vm.isLoading = false;
    	vm.assistents=[];
      vm.goToassistent = goToassistent;
      vm.doSendEmail = doSendEmail;


        vm.showList = [ ];
    $interval(function() {
      vm.determinateValue += 4;

      if (vm.determinateValue > 100) vm.determinateValue = 30;

        // Incrementally start animation the five (5) Indeterminate,
        // themed progress circular bars

        if ( (j < 2) && !vm.showList[j] && vm.activated ) {
          vm.showList[j] = true;
        }
        if ( counter++ % 4 == 0 ) j++;

        // Show the indicator in the "Used within Containers" after 200ms delay
        if ( j == 2 ) vm.contained = "indeterminate";

    }, 100, 0, true);

    $interval(function() {
      vm.mode = (vm.mode == 'query' ? 'determinate' : 'query');
    }, 7200, 0, true);
        /**
         * Turn off or on the 5 themed loaders
         */
        vm.toggleActivation = function() {
            if ( !vm.activated ) vm.showList = [ ];
            if (  vm.activated ) {
              j = counter = 0;
              vm.determinateValue = 30;
              vm.determinateValue2 = 30;
            }
        };
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color;
            $scope.color_font_toolbar = event.color_font_toolbar;
            $rootScope.color_font_toolbar = $scope.color_font_toolbar;
             $timeout(function(){ 
                JsavRestangular.one('events',vm.event_id).all('assistents').getList().then(function (assistents) {
                    vm.assistents = assistents;
                    vm.determinateValue = 100;
                    vm.isLoading = true;

                });
            },1500);

		    }


        
		  }
      function goToassistent(assistent){
          $location.path('/asistentes/'+assistent+"/");
      }
      function doSendEmail(email){
          $window.location = "mailto:" + email;
      }


		
    }

})();