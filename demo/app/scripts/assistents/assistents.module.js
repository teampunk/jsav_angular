(function () {
  'use strict';

  angular
    .module('jsav.assistents', [
      'jsav.assistents.controllers',
      'jsav.assistents.services',
      'jsav.assistents.directives',

    ]);

  angular
    .module('jsav.assistents.controllers', []);

  angular
    .module('jsav.assistents.services', []);

   angular
    .module('jsav.assistents.directives', []);

})();