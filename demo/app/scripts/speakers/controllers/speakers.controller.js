/**
* SpeakersController
* @namespace jsav.speakers.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.speakers.controllers')
    .controller('SpeakersController', SpeakersController);
  	

  	SpeakersController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function SpeakersController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.speakers=[];
    	vm.texto = {
    		title: 'Conferencistas',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

            JsavRestangular.one('events',vm.event_id).all('speakers').getList().then(function (speakers) {
                    vm.loading = false;
        			vm.speakers = speakers;
    			});
		    }

        
		  }
		
    }

})();