/**
* SpeakerDetail
* @namespace jsav.speakers.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.speakers.controllers')
    .controller('SpeakerDetail', SpeakerDetail);
    

    SpeakerDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function SpeakerDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;
      vm.single_speaker={};


      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
              JsavRestangular.one('events',vm.event_id).one('speakers', $stateParams.id).get().then(function (speakers) {
                vm.loading = false;
                vm.single_speaker = speakers;
                vm.texto = {
                   title:vm.single_speaker.name,
                   url: 'agenda',
              };
            })
          }
      }
    }

})();