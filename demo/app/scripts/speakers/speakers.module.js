(function () {
  'use strict';

  angular
    .module('jsav.speakers', [
      'jsav.speakers.controllers',
      'jsav.speakers.services'

    ]);

  angular
    .module('jsav.speakers.controllers', []);

  angular
    .module('jsav.speakers.services', []);

})();