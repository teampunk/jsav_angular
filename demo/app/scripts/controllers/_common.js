'use strict';

/**
 * @ngdoc function
 * @name demoApp.controller:CommonCtrl
 * @description
 * # CommonCtrl
 * Controller of the demoApp
 */
angular.module('jsav')
    .controller('CommonCtrl', [
        '$scope',
        '$mdSidenav',
        '$timeout',
        '$rootScope',
        '$state',
        'ssSideNav',
        'ssSideNavSharedService',
        'Authentication',
        '$translate',
        '$location',
        function (
            $scope,
            $mdSidenav,
            $timeout,
            $rootScope,
            $state,
            ssSideNav,
            ssSideNavSharedService,
            Authentication,
            $translate,
            $location

            ) {
            var vm = this;

            $scope.onClickMenu = function () {
                $mdSidenav('left').toggle();
            };

            $scope.menu = ssSideNav;

            $rootScope.event = Authentication.getAuthenticatedAccount();
            $scope.getLanguages = function(){
                Authentication.getLanguages();
             };

            $scope.setLanguages = function(selected_language){
                Authentication.setLanguages(selected_language);
            };

            $scope.changeLanguage = function (langKey) {
                $translate.use(langKey);
                $mdSidenav('left').toggle();
                $location.path('/');

            };
            
           
          
            

        }
    ]);