/**
* LoginController
* @namespace jsav.authentication.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav')
    .controller('Footer', Footer)

Footer.$inject = ['$scope', 'Authentication'];

function Footer($scope, Authentication){
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();
    // vm.id = $stateParams;
    vm.event = Authentication.getAuthenticatedAccount();
    activate();
    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf jsav.authentication.controllers.LoginController
    */
        function activate() {
            if (!vm.event) {

            }
        }
        // if (vm.isAuthenticated) {
        //         vm.logo_hotel = vm.event.logo_hotel.split('/');
        //         if (vm.logo_hotel.length > 5 ) {
        //             vm.logo_footer = true;
        //             console.log(vm.logo_footer);
        //         }
        //         else{
        //             vm.logo_footer = false;   
        //             console.log(vm.logo_footer);
        //         }
            
        // }

    }

})();