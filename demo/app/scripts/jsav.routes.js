(function () {
  'use strict';



  function config($urlRouterProvider, $stateProvider) {

          $urlRouterProvider.otherwise(function () {
                return '/login/';
            });
            $stateProvider.state({
                name: 'common',
                abstract: true,
                templateUrl: 'views/_common.html',
                controller: 'CommonCtrl'
            });
            $stateProvider.state({
                name: 'common.event',
                url: '/',
                templateUrl: 'views/home/home.html',
                controller: 'HomeController as vm',
            });


            $stateProvider.state({
                name: 'common.home',
                url: '/login/',
                templateUrl: 'views/authentication/login.html',
                controller: 'LoginController as vm'
                // controllerAs: 'vm',
            });
            $stateProvider.state({
                name: 'common.facilidades',
                url: '/facilidades/',
                templateUrl: 'views/facilidades/facilidades.html',
                controller: 'FacilitiesController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    },
                    facilities:function(Authentication, JsavRestangular){
                        var vm = this;
                        var event = Authentication.getAuthenticatedAccount();
                        vm.event_id = event.pk;
                        return JsavRestangular.one('events', vm.event_id).getList('facilities');

                    }
                }
            });
             $stateProvider.state({
                name: 'agenda',
                abstract:true,
                templateUrl: 'views/conferences/conferences.html',
                controller: 'CommonCtrl as vm',
                 resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                }
            });
            $stateProvider.state({
                name: 'common.agenda',
                url: '/agenda/',
                // parent: 'conferencias',
                templateUrl: 'views/conferences/conference-list.html',
                controller: 'ConferenceController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.detail',
                url: '/agenda/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/conferences/conferences-detail.html',
                controller: 'Detail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.speakers',
                url: '/conferencistas/',
                templateUrl: 'views/speakers/speakers-list.html',
                controller: 'SpeakersController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.speakersdetail',
                url: '/conferencistas/:id/',
                templateUrl: 'views/speakers/speakers-detail.html',
                controller: 'SpeakerDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.assistents',
                url: '/asistentes/',
                templateUrl: 'views/assistents/assistents-list.html',
                controller: 'AssistentsController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.asistentdetail',
                url: '/asistentes/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/assistents/assistents-detail.html',
                controller: 'AssistentDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.links',
                url: '/links/',
                templateUrl: 'views/links/links-list.html',
                controller: 'LinkController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.linkdetail',
                url: '/links/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/links/links-detail.html',
                controller: 'LinkDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });


            $stateProvider.state({
                name: 'common.downloads',
                url: '/descargas/',
                templateUrl: 'views/downloads/downloads-list.html',
                controller: 'DownloadController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.downloadsdetail',
                url: '/descargas/:id/',
                // parent: 'conferencias',
                templateUrl: 'views/downloads/downloads-detail.html',
                controller: 'DownloadDetail as vm',
            });

            $stateProvider.state({
                name: 'common.newsletters',
                url: '/boletines/',
                templateUrl: 'views/newsletters/newsletters-list.html',
                controller: 'NewslettersController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.newslettersdetail',
                url: '/boletines/:id/',
                templateUrl: 'views/newsletters/newsletters-detail.html',
                controller: 'NewsletterDetail as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });

            $stateProvider.state({
                name: 'common.tips',
                url: '/tips/',
                templateUrl: 'views/tips/tips-list.html',
                controller: 'TipsController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.tipsdetail',
                url: '/tips/:id/',
                templateUrl: 'views/tips/tips-detail.html',
                controller: 'TipDetailController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });

            $stateProvider.state({
                name: 'common.sponsors',
                url: '/patrocinadores/',
                templateUrl: 'views/sponsors/sponsors-list.html',
                controller: 'SponsorsController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });
            $stateProvider.state({
                name: 'common.sponsorsdetail',
                url: '/patrocinadores/:id/',
                templateUrl: 'views/sponsors/sponsors-detail.html',
                controller: 'SponsorDetailController as vm',
                resolve: {
                    delete_token: function(djangoAuth){
                        djangoAuth.delete_token();
                    
                    }
                },
            });

          
            $stateProvider.state({
                name: 'common.ajustes',
                url: '/ajustes/',
                templateUrl: 'views/auth/login.html',
                controller:'LoginCtrl as vm',
                resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                      
                    }
            });

            $stateProvider.state({
                name: 'common.logout',
                resolve: {function(Authentication)
                    {
                        Authentication.unauthenticate();
                    }
                }
            });

                $stateProvider.state({
                    name: 'common.loginauth',
                    url: '/login-auth/',
                    templateUrl: 'views_auth/main.html',
                    controller: 'MainCtrl',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }]
                    }
                });
                $stateProvider.state({
                    name: 'common.register',
                    url: '/registro/',
                    templateUrl: 'views/auth/register.html',
                    controller: 'RegisterCtrl as vm',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                            return djangoAuth.authenticationStatus();
                        }],
                    }
                });
                $stateProvider.state({
                    name: 'common.passwordreset',
                    url: '/passwordReset',
                    templateUrl: 'views_auth/passwordReset.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.passwordResetConfirm',
                    url: '/passwordResetConfirm',
                    templateUrl: 'views_auth/passwordresetconfirm.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.login',
                    url: '/login-auth/login',
                    templateUrl: 'views_auth/login.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.emailVerificationToken',
                    url: '/verifyEmail/:emailVerificationToken',
                    templateUrl: 'views_auth/verifyemail.html',
                    resolve: {
                       authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.logoutauth',
                    url: '/logout-auth',
                    templateUrl: 'views_auth/logout.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.logout();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.userProfile',
                    url: '/userProfile',
                    templateUrl: 'views/auth/userprofile.html',
                    controller:'UserprofileCtrl as vm',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                        authenticated: ['djangoAuth', function(djangoAuth){
                        console.log(djangoAuth.authenticationStatus());
                      }],

                    }
                });
                $stateProvider.state({
                    name: 'common.passwordChange',
                    url: '/passwordChange',
                    controller: 'PasswordchangeCtrl as vm',
                    templateUrl: 'views/auth/passwordchange.html',
                    resolve: {
                       authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.restricted',
                    url: '/restricted',
                    controller: 'RestrictedCtrl',
                    templateUrl: 'views_auth/restricted.html',
                    resolve: {
                       authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus();
                      }],
                    }
                });
                $stateProvider.state({
                    name: 'common.authRequired',
                    url: '/authRequired',
                    controller: 'AuthrequiredCtrl',
                    templateUrl: 'views_auth/authrequired.html',
                    resolve: {
                      authenticated: ['djangoAuth', function(djangoAuth){
                        return djangoAuth.authenticationStatus(true);
                      }],
                    }
                });                                
  

  
  }
  config.$inject = [ '$urlRouterProvider', '$stateProvider'];         
  
  angular
    .module('jsav.routes')
    .config(config);


})();
