/**
* SponsorDetailController
* @namespace jsav.sponsors.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.sponsors.controllers')
    .controller('SponsorDetailController', SponsorDetailController);
    

    SponsorDetailController.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams', '$rootScope'];

    function SponsorDetailController ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;
      vm.single_sponsors={};


      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
              JsavRestangular.one('events',vm.event_id).one('sponsors', $stateParams.id).get().then(function (sponsors) {
                vm.loading = false;
                vm.single_sponsors = sponsors;
                vm.texto = {
                   title:vm.single_sponsors.name,
                   url: 'conferencias',
              };
            })
          }
      }
    }

})();