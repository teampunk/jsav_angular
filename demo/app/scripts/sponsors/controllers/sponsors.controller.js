/**
* SponsorsController
* @namespace jsav.speakers.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.sponsors.controllers')
    .controller('SponsorsController', SponsorsController);
  	

  	SponsorsController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function SponsorsController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.sponsors={};
    	vm.texto = {
    		title: 'Patrocinadores',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color;
             
		        JsavRestangular.one('events',vm.event_id).one('sponsors').get().then(function (sponsors) {
                    vm.loading = false;
        			vm.sponsors = sponsors;
    			});
		    }

        
		  }
		
    }

})();