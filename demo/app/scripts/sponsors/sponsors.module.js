(function () {
  'use strict';

  angular
    .module('jsav.sponsors', [
      'jsav.sponsors.controllers',
      'jsav.sponsors.services'

    ]);

  angular
    .module('jsav.sponsors.controllers', []);

  angular
    .module('jsav.sponsors.services', []);

})();