(function () {
  'use strict';

  function config( 
            $mdThemingProvider,
            $locationProvider,
            ssSideNavSectionsProvider,
            $translateProvider,
            $httpProvider)
  {

          $translateProvider.useStaticFilesLoader({
            prefix: 'languages/locale-',
            suffix: '.json'
          });
          $translateProvider.preferredLanguage('es');
          // $translateProvider.useLocalStorage();
          $translateProvider.useCookieStorage();
          $translateProvider.useSanitizeValueStrategy('sanitize');

          $httpProvider.defaults.xsrfCookieName = 'csrftoken';
          $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

          $mdThemingProvider
              .theme('default')
              .primaryPalette('blue-grey', {
                  'default': '800',
                  //       'default': '400', // by default use shade 400 from the pink palette for primary intentions
                  //       'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                  //       'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                  //       'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
              });

    
            ssSideNavSectionsProvider.initWithTheme($mdThemingProvider);
            ssSideNavSectionsProvider.initWithSections([
              {
                id: 'inicio',
                name: 'INICIO',
                state: 'common.event',
                type: 'link',
            },
            {
                id: 'facilidades',
                name: 'FACILIDADES',
                state: 'common.facilidades',
                type: 'link',
            },
            {
                id: 'agenda',
                name: 'AGENDA',
                state: 'common.agenda',
                type: 'link',
            },{
                id: 'speakers',
                name: 'CONFERENCISTAS',
                state: 'common.speakers',
                type: 'link',
            },{
                id: 'assistents',
                name: 'ASISTENTES',
                state: 'common.assistents',
                type: 'link',
            },{
                id: 'links',
                name: 'LINKS',
                state: 'common.links',
                type: 'link',
            },{
                id: 'descargas',
                name: 'DESCARGAS',
                state: 'common.downloads',
                type: 'link',
            },{
                id: 'newsletters',
                name: 'BOLETINES',
                state: 'common.newsletters',
                type: 'link',
            },{
                id: 'tips',
                name: 'TIPS',
                state: 'common.tips',
                type: 'link',
            }, {
                id: 'patrocinadores',
                name: 'PATROCINADORES',
                state: 'common.sponsors',
                type: 'link',
            }, {
                id: 'ajustes',
                name: 'AJUSTES',
                state: 'common.ajustes',
                type: 'link'
            }, {
                id: 'logout',
                name: 'CERRAR_SESION',
                state: 'common.logout',
                type: 'link',
            }, 
        ]);
  }
   config.$inject = [
        '$mdThemingProvider',
        '$locationProvider',
        'ssSideNavSectionsProvider',
        '$translateProvider',
        '$httpProvider'
        ];
  
  angular
    .module('jsav.config')
      .config(config);
})();