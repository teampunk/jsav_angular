(function () {
    'use strict';

/**
 * @ngdoc overview
 * @name demoApp
 * @description
 * # demoApp
 *
 * Main module of the application.
 */
angular
    .module('jsav', [
        'jsav.auth',
        'ngAnimate',
        'ngAria',
        'ngMaterial',
        'jsav.config',
        'jsav.routes',
        'jsav.constants',
        'jsav.authentication',
        'jsav.home',
        'jsav.facilities',
        'jsav.conferences',
        'jsav.assistents',
        'jsav.speakers',
        'jsav.links',
        'jsav.downloads',
        'jsav.newsletters',
        'jsav.tips',
        'jsav.sponsors',

    ]);
    angular
        .module('jsav.routes', ['ngRoute','ui.router','ngResource','ngCookies','sasrio.angular-material-sidenav', 'ngAria', 'restangular', 'ngAnimate'] );

     angular
        .module('jsav.config', ['restangular', 'sasrio.angular-material-sidenav', 'ngSanitize','ngMessages','ngCookies', 'pascalprecht.translate', 'ngAnimate']);

    angular
        .module('jsav.constants', []);

    angular
        .module('jsav')
        .run(function($http, djangoAuth){
            $http.defaults.xsrfHeaderName = 'X-CSRFToken';
            $http.defaults.xsrfCookieName = 'csrftoken';
            djangoAuth.initialize('//localhost:8005/rest-auth', false);
        });
})();