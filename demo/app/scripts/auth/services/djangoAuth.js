'use strict';

angular.module('jsav.auth.services')
  .service('djangoAuth', function djangoAuth($q, $http, $cookies, $rootScope, Authentication, $location) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    if (!Authentication.isAuthenticated()) {
        $location.path('/login');
    }
    else{
        var event = Authentication.getAuthenticatedAccount();
        var id = event.pk;
    }
    
    console.log(id);
    var API_URL = "//localhost:8005/rest-auth";
    var API_URL_ASSISTENT = "//localhost:8005/v1";
    var service = {
        /* START CUSTOMIZATION HERE */
        // Change this to point to your Django REST Auth API
        // e.g. /api/rest-auth  (DO NOT INCLUDE ENDING SLASH)

        'API_URL': '//localhost:8005/rest-auth',
        'API_URL_ASSISTENT':'//localhost:8005/v1',

        // Set use_session to true to use Django sessions to store security token.
        // Set use_session to false to store the security token locally and transmit it as a custom header.
        'use_session': true,
        /* END OF CUSTOMIZATION */
        'authenticated': null,
        'authPromise': null,
        'authenticated_event':null,

        'request': function(args, api_url) {
            // Let's retrieve the token from the cookie, if available
            if($cookies.getObject('token')){
                $http.defaults.headers.common.Authorization = 'Token ' + $cookies.getObject('token');
            }
            
            // Continue
            params = args.params || {}
            args = args || {};
            var deferred = $q.defer(),
                url = api_url + args.url,
                method = args.method || "GET",
                params = params,
                data = args.data || {};
            // Fire the request, as configured.
            $http({
                url: url,
                withCredentials: this.use_session,
                method: method.toUpperCase(),
                headers: {
                        'X-CSRFToken': $cookies['csrftoken'],
                        
                },
                params: params,
                data: data
            })
            .success(angular.bind(this,function(data, status, headers, config) {
                deferred.resolve(data, status);
            }))
            .error(angular.bind(this,function(data, status, headers, config) {
                console.log("error syncing with: " + url);


                // Set request status
                if(data){
                    data.status = status;
                }
                if(status == 0){
                    if(data == ""){
                        data = {};
                        data['status'] = 0;
                        data['non_field_errors'] = ["Could not connect. Please try again."];
                    }
                    // or if the data is null, then there was a timeout.
                    if(data == null){
                        // Inject a non field error alerting the user
                        // that there's been a timeout error.
                        data = {};
                        data['status'] = 0;
                        data['non_field_errors'] = ["Server timed out. Please try again."];
                    }
                }
                deferred.reject(data, status, headers, config);
            }));
            return deferred.promise;
        },
        'register': function(username,password,email,first_name, last_name, event, show_assistent, company,job, biography, more){
            var data = {
                'username':username,
                'password':password,
                'email':email,
                'first_name': first_name,
                'last_name': last_name,
                'event': event,
                'show_assistent': show_assistent,
                'company': company,
                'job': job,
                'biography': biography

            }
            // data = angular.extend(data,more);
            var event = Authentication.getAuthenticatedAccount();
            var id = event.pk;
            return this.request({
                'method': "POST",
                'url': "/events/"+ id +"/assistents/",
                'data' :data,
            },API_URL_ASSISTENT);
        },
        'login': function(username,password){
            var djangoAuth = this;
            return this.request({
                'method': "POST",
                'url': "/login/",
                'data':{
                    'username':username,
                    'password':password
                }
            },this.API_URL).then(function(data){
                if(!djangoAuth.use_session){
                    $http.defaults.headers.common.Authorization = 'Token ' + data.key;
                    $cookies.putObject('token', data.key);
                }
                djangoAuth.authenticated = true;

                $rootScope.$broadcast("djangoAuth.logged_in", data);
                $location.path('/userProfile');
            });
        },
        'logout': function(){
            var djangoAuth = this;
            return this.request({
                'method': "POST",
                'url': "/logout/"
            },this.API_URL).then(function(data){
                delete $http.defaults.headers.common.Authorization;
                // delete $cookies.token;
                $cookies.remove('token');
                djangoAuth.authenticated = false;
                $rootScope.$broadcast("djangoAuth.logged_out");
                $location.path('/ajustes/');
            });
        },
        'changePassword': function(password1,password2){
            return this.request({
                'method': "POST",
                'url': "/password/change/",
                'data':{
                    'new_password1':password1,
                    'new_password2':password2
                }
            },this.API_URL);
        },
        'resetPassword': function(email){
            return this.request({
                'method': "POST",
                'url': "/password/reset/",
                'data':{
                    'email':email
                }
            }, this.API_URL);
        },
        'profile': function(){
            return this.request({
                'method': "GET",
                'url': "/events/"+id+"/assistents/?detail=true",
            }, API_URL_ASSISTENT); 
        },
        'updateProfile': function(data){
            console.log("updateProfile");
            console.log(data);
            var event = Authentication.getAuthenticatedAccount();
            var id = event.pk;
            console.log($cookies.getObject('assistent_id'))
            return this.request({
                'method': "PUT",
                'url': "/events/"+id+"/assistents/"+ $cookies.getObject('assistent_id') +"/",
                'data':data
            }, this.API_URL_ASSISTENT); 
        },
        'verify': function(key){
            return this.request({
                'method': "POST",
                'url': "/registration/verify-email/",
                'data': {'key': key} 
            }, this.API_URL);            
        },
        'confirmReset': function(uid,token,password1,password2){
            return this.request({
                'method': "POST",
                'url': "/password/reset/confirm/",
                'data':{
                    'uid': uid,
                    'token': token,
                    'new_password1':password1,
                    'new_password2':password2
                }
            }, this.API_URL);
        },
        'authenticationStatus': function(restrict, force){
            djangoAuth.authenticated_event = Authentication.isAuthenticated();
            // Set restrict to true to reject the promise if not logged in
            // Set to false or omit to resolve when status is known
            // Set force to true to ignore stored value and query API
            restrict = restrict || false;
            force = force || false;
            if(this.authPromise == null || force){
                this.authPromise = this.request({
                    'method': "GET",
                    'url': "/user/"
                },this.API_URL)

            }
            var da = this;
            var getAuthStatus = $q.defer();
            if(this.authenticated != null && !force){
                // We have a stored value which means we can pass it back right away.
                if(this.authenticated == false && restrict){
                    getAuthStatus.reject("User is not logged in.");
                    if (!djangoAuth.authenticated_event) {
                        $location.path('/');
                    }
                    
                }else{
                    getAuthStatus.resolve();
                }
            }else{
                // There isn't a stored value, or we're forcing a request back to
                // the API to get the authentication status.
                this.authPromise.then(function(){
                    da.authenticated = true;
                    getAuthStatus.resolve();
                },function(){
                    da.authenticated = false;
                    if(restrict){
                        getAuthStatus.reject("User is not logged in.");
                        if (!djangoAuth.authenticated_event) {
                            $location.path('/login');
                        }
                    }else{
                        getAuthStatus.resolve();
                    }
                });
            }
            return getAuthStatus.promise;
        },
        'initialize': function(url, sessions){
            this.API_URL = url;
            this.use_session = sessions;
            return this.authenticationStatus();
        },
        'delete_token':function(){

            delete $http.defaults.headers.common.Authorization;
            if ($cookies.getObject('NG_TRANSLATE_LANG_KEY')) {
                var language = $cookies.getObject('NG_TRANSLATE_LANG_KEY');
                $http.defaults.headers.common['Accept-Language'] = language;
            }
            

        },
        'add_token':function(){
            if($cookies.getObject('token')){
                $http.defaults.headers.common.Authorization = 'Token ' + $cookies.getObject('token');
            }
        }

    }
    return service;
  });
