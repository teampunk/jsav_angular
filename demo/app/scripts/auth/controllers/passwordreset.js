'use strict';

angular.module('jsav.auth.controllers')
  .controller('PasswordresetCtrl', function ($scope,$rootScope, Authentication, djangoAuth, Validate) {
    $scope.model = {'email':''};
  	$scope.complete = false;

    var event = Authentication.getAuthenticatedAccount();
    $scope.toolbar_color = event.toolbar_color;
    $rootScope.toolbar_color = $scope.toolbar_color;
    $scope.menu_color = event.menu_color;
    $rootScope.menu_color = $scope.menu_color;
    $scope.title_color = event.title_color;
    $rootScope.title_color = $scope.title_color; 
    $scope.resetPassword = function(formData){
      $scope.errors = [];
      Validate.form_validation(formData,$scope.errors);
      if(!formData.$invalid){
        djangoAuth.resetPassword($scope.model.email)
        .then(function(data){
        	// success case
        	$scope.complete = true;
        },function(data){
        	// error case
        	$scope.errors = data;
        });
      }
    }
  });
