'use strict';

angular.module('jsav.auth.controllers')
  .controller('LogoutCtrl', function ($scope, $location, djangoAuth) {
    djangoAuth.logout();
  });
