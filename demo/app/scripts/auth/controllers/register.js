(function(){
  'use strict';

angular.module('jsav.auth.controllers')
  .controller('RegisterCtrl', function ($scope, $location, djangoAuth, Validate, $cookies,Authentication) {
    var vm = this;

  	// vm.model = {'username':'','password':'','email':''};

  	vm.complete = false;

    vm.authenticated = false;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
    });
    // Wait and respond to the logout event.
    $scope.$on('djangoAuth.logged_out', function() {
      vm.authenticated = false;
    });
    // Wait and respond to the log in event.
    $scope.$on('djangoAuth.logged_in', function() {
      vm.authenticated = true;
    });
    
$scope.uploadFile = function(files) {
    var fd = new FormData();
    //Take the first selected file
    fd.append("file", files[0]);
    console.log(files[0]);
    vm.model.photo = files[0];

};

    vm.register = function(formData){
        vm.errors = [];
      // vm.model.photo = $scope.fileName

      Validate.form_validation(formData,vm.errors);
    
      if(!formData.$invalid){
        var event = Authentication.getAuthenticatedAccount();
        vm.model.event = event.pk;
        djangoAuth.register( 
          vm.model.username,
          vm.model.password,
          vm.model.email,
          vm.model.fist_name,
          vm.model.last_name,
          vm.model.event,  
          vm.model.show_assistent,
          vm.model.company,
          vm.model.job,
          vm.model.biography,
          $scope.fileName
        )
        .then(function(data){
        	// success case
        	vm.complete = true;
          
          
        },function(data){
        	// error case
        	vm.errors = data;
          console.log(data);
        });
      }
    }
  });

})();