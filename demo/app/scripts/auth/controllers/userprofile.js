'use strict';

angular.module('jsav.auth.controllers')
  .controller('UserprofileCtrl', function ($scope,$rootScope, djangoAuth, Validate, Authentication, $cookies, $window) {
    
    var vm = this;
    vm.complete = false;    
    vm.authenticated = false;

    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
        // vm.model = {'username':'', 'first_name':'','last_name':'','email':'','event':'','company':'', 'job':'', 'biography':''};

        var event = Authentication.getAuthenticatedAccount();
        vm.event_id = event.pk;
        $scope.toolbar_color = event.toolbar_color;
        $rootScope.toolbar_color = $scope.toolbar_color;
        $scope.menu_color = event.menu_color;
        $rootScope.menu_color = $scope.menu_color;
        $scope.title_color = event.title_color;
        $rootScope.title_color = $scope.title_color; 

        djangoAuth.profile().then(function(data){
            vm.model = data;
            console.log(data);
            console.log("data assistent_id");
            $window.localStorage.setItem('assistent_id',data.assistent_id);
            $cookies.putObject('assistent_id',data.assistent_id);
            console.log(data);
            console.log($cookies.getObject('assistent_id'));

        });
    });

     // Wait and respond to the logout event.
    $scope.$on('djangoAuth.logged_out', function() {
      vm.authenticated = false;
    });
    // Wait and respond to the log in event.
    $scope.$on('djangoAuth.logged_in', function() {
      vm.authenticated = true;
    });
    
    vm.changeComplete = function(param){
      vm.complete = param;
    }

    vm.updateProfile = function(formData, model){
      vm.errors = [];
      Validate.form_validation(formData, vm.errors);
      if(!formData.$invalid){
        djangoAuth.updateProfile(model)
        .then(function(data){
        	// success case
        	vm.complete = false;
          vm.error.username = ""
        },function(data){
        	// error case
        	vm.error = data;
          console.log("data");
          console.log(data);
        });
      }
    }
  });
