'use strict';

angular.module('jsav.auth.controllers')
  .controller('LoginCtrl', function ($scope, $rootScope,$location, djangoAuth, Validate, Authentication, authenticated) {
    var vm = this;
  	vm.complete = false;
    vm.login = login;
    vm.authenticated = false;
    vm.greeting = authenticated;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
        $location.path('userProfile');
    });

    if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
    };
    function login (formData){
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
      if(!formData.$invalid){
        djangoAuth.login(vm.username, vm.password)
        .then(function(data){
        	// success case
        	// $location.path("/ajustes");
          vm.complete = true;
        },function(data){
        	// error case
        	vm.errors = data;
        });
      }
    }
  });
