'use strict';

angular.module('jsav.auth.controllers')
  .controller('PasswordchangeCtrl', function ($scope,$rootScope,Authentication, djangoAuth, Validate) {
    var vm = this;
    vm.model = {'new_password1':'','new_password2':''};
    vm.complete = false;
    vm.authenticated = false;
    // Wait for the status of authentication, set scope var to true if it resolves
    djangoAuth.authenticationStatus(true).then(function(){
        vm.authenticated = true;
    });
    var event = Authentication.getAuthenticatedAccount();
    $scope.toolbar_color = event.toolbar_color;
    $rootScope.toolbar_color = $scope.toolbar_color;
    $scope.menu_color = event.menu_color;
    $rootScope.menu_color = $scope.menu_color;
    $scope.title_color = event.title_color;
    $rootScope.title_color = $scope.title_color; 

    vm.changePassword = function(formData){
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
      if(!formData.$invalid){
        djangoAuth.changePassword(vm.model.new_password1, vm.model.new_password2)
        .then(function(data){
        	// success case
        	vm.complete = true;
        },function(data){
        	// error case
        	vm.errors = data;
        });
      }
    }
  });
