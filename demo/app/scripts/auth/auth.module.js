(function () {
  'use strict';

  angular
    .module('jsav.auth', [
      'jsav.auth.controllers',
      'jsav.auth.services'
    ]);

  angular
    .module('jsav.auth.controllers', ['ngMessages']);

  angular
    .module('jsav.auth.services', ['ngCookies']);
})();