(function () {
  'use strict';

  angular
    .module('jsav.newsletters', [
      'jsav.newsletters.controllers',
      'jsav.newsletters.services'

    ]);

  angular
    .module('jsav.newsletters.controllers', []);

  angular
    .module('jsav.newsletters.services', []);

})();