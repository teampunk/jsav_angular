/**
* NewsletterDetailConferencesController
* @namespace jsav.conference.NewsletterController.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.newsletters.controllers')
    .controller('NewsletterDetail', NewsletterDetail);
  	

  	NewsletterDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function NewsletterDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.single_newsletter =[];


    	activate();
		
		function activate() {

		  if (Authentication.isAuthenticated()) {
			var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
	        $stateParams.id
	        // console.log($stateParams);
	        JsavRestangular.one('events',vm.event_id).one('newsletters', $stateParams.id).get().then(function (newsletter) {
                vm.loading = false;
    			vm.single_newsletter = newsletter;
    			vm.texto = {
                    title: vm.single_newsletter.name,
				};
			})
	       }
		}
    }

})();