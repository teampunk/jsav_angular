/**
* Facility
* @namespace jsav.facility.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.facilities.services')
    .factory('JsavRestangular', JsavRestangular);

    JsavRestangular.$inject = ['Restangular', 'Authentication', '$http','$cookies'];
    
    function JsavRestangular(Restangular, Authentication, $http, $cookies){
      delete $http.defaults.headers.common.Authorization;
      var vm = this;
      vm.language = Authentication.getLanguages();
      return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl('http://localhost:8005/v1/');
    });

    }

})();