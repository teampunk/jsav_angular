(function () {
  'use strict';

  angular
    .module('jsav.facilities', [
      'jsav.facilities.controllers',
      'jsav.facilities.services'

    ]);

  angular
    .module('jsav.facilities.controllers', ['uiGmapgoogle-maps', 'ngResource']);

  angular
    .module('jsav.facilities.services', []);
})();