/**
* LoginController
* @namespace jsav.facilities.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.facilities.controllers', ['uiGmapgoogle-maps'])
    .controller('FacilitiesController', FacilitiesController);
  	

  	FacilitiesController.$inject = ['$cookies', 'Authentication', 'JsavRestangular', '$stateParams','uiGmapGoogleMapApi', 'facilities', '$scope', '$rootScope'];

    function FacilitiesController ($cookies, Authentication, JsavRestangular, $stateParams, uiGmapGoogleMapApi, facilities, $scope, $rootScope){
    	var vm = this;
    	vm.texto = {
    		title: 'Facilidades',
    		location:'Ubicación',
    	};
    	vm.facility={};

    	activate();
		function activate() {

			if (Authentication.isAuthenticated()) {
                var event = Authentication.getAuthenticatedAccount();
                $scope.toolbar_color = event.toolbar_color;
                $rootScope.toolbar_color = $scope.toolbar_color;
                $scope.menu_color = event.menu_color;
                $rootScope.menu_color = $scope.menu_color;
                $scope.title_color = event.title_color;
                $rootScope.title_color = $scope.title_color; 
        				
                vm.facility = facilities[0];
                vm.location=vm.facility.location.split(',');
        				vm.map = {center: {latitude:vm.location[0], longitude: vm.location[1] }, zoom: 12 };
        		     	vm.options = {scrollwheel: false};
        		     	vm.marker = vm.map.center;
        		    }
		}
		 
    }

})();