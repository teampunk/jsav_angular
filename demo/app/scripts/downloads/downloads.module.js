(function () {
  'use strict';

  angular
    .module('jsav.downloads', [
      'jsav.downloads.controllers',
      'jsav.downloads.services'

    ]);

  angular
    .module('jsav.downloads.controllers', []);

  angular
    .module('jsav.downloads.services', []);

})();