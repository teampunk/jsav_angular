/**
* DownloadDetail
* @namespace jsav.download.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.conferences.controllers')
    .controller('DownloadDetail', DownloadDetail);
    

    DownloadDetail.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function DownloadDetail ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
      var vm = this;
      vm.loading = true;    
      vm.single_conrence=[];


      activate();
    
    function activate() {

      if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

            $stateParams.id
            // console.log($stateParams);
            JsavRestangular.one('events',vm.event_id).one('conferences', $stateParams.id).get().then(function (conferences) {
              vm.loading = false;    
              vm.single_conrence = conferences;
              vm.texto = {
              title:vm.single_conrence.name,
              conferencistas: 'Descargas',
              links: 'Descargas',
              descargas: 'Descargas',
            };
          })
        }
    }
    }

})();