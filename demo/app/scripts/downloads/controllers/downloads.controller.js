/**
* /**
* DownloadController
* @namespace jsav.downloads.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.downloads.controllers')
    .controller('DownloadController', DownloadController);
    

    DownloadController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function DownloadController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
      var vm = this;
      vm.loading = true;    
      vm.conferences=[];
      vm.texto = {
        title: 'Descargas',
        url: 'descargas',
      };
      // var event = Authentication.getAuthenticatedAccount();
      
      activate();
    
    function activate() {

       if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 
              
            JsavRestangular.one('events',vm.event_id).all('conferences').getList().then(function (conferences) {
              vm.loading = false;    
              vm.conferences = conferences;
          });
      
        }
      }
    
    }

})();