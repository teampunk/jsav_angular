/**
* TipDetailController
* @namespace jsav.tips.detail.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.tips.controllers')
    .controller('TipDetailController', TipDetailController);
  	

  	TipDetailController.$inject = ['$location','$scope', 'Authentication', 'JsavRestangular', '$stateParams','$rootScope'];

    function TipDetailController ($location, $scope, Authentication, JsavRestangular, $stateParams, $rootScope){
    	var vm = this;
    	vm.single_tip=[];


    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

		        JsavRestangular.one('events',vm.event_id).one('tips', $stateParams.id).get().then(function (tips) {
        			vm.single_tip = tips;
        			vm.texto = {
    					   title:vm.single_tip.name,
    				};
    			})
		    }
		}
    }

})();