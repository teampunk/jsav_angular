/**
* TipsController
* @namespace jsav.conference.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.tips.controllers')
    .controller('TipsController', TipsController);
  	

  	TipsController.$inject = ['$location', 'Authentication', 'JsavRestangular', '$stateParams', '$scope', '$rootScope'];

    function TipsController ($location, Authentication, JsavRestangular, $stateParams, $scope, $rootScope){
    	var vm = this;
        vm.loading = true;
    	vm.conferences=[];
    	vm.texto = {
    		title: 'Tips',
        url: 'tips',
    	};
    	activate();
		
		function activate() {

			if (Authentication.isAuthenticated()) {
            var event = Authentication.getAuthenticatedAccount();
            vm.event_id =  event.pk;
            
            $scope.toolbar_color = event.toolbar_color;
            $rootScope.toolbar_color = $scope.toolbar_color;
            $scope.menu_color = event.menu_color;
            $rootScope.menu_color = $scope.menu_color;
            $scope.title_color = event.title_color;
            $rootScope.title_color = $scope.title_color; 

		        JsavRestangular.one('events',vm.event_id).all('tips').getList().then(function (tips) {
                    vm.loading = false;
        			vm.conferences = tips;
    			});

		    }
		  }
		
    }

})();