/**
* tips
* @namespace jsav.tips.directives
*/
(function(){
	'use strict';


	angular
		.module('jsav.conferences.directives')
		.directive('conferences', conferences);
		/**
  		* @namespace conferences
  		*/

  	function conferences(){
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf jsav.conferences.directives.Conferences
    */  			
    var directive = {
      controller: 'ConferencesController',
      controllerAs: 'vm',
      restrict: 'E',
      scope: {
        conferences: '='
      },
      templateUrl: 'templates/conferences/conferences-directive.html'
    };    

    return directive;
    
  	}
})();