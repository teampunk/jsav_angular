(function () {
  'use strict';

  angular
    .module('jsav.tips', [
      'jsav.tips.controllers',
      'jsav.tips.directives',
      'jsav.tips.services'

    ]);

  angular
    .module('jsav.tips.controllers', []);

   angular
    .module('jsav.tips.directives', []);

  angular
    .module('jsav.tips.services', []);

})();