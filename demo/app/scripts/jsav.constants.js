(function () {
	'use strict';

    var app = angular.module("jsav");

    var JsavConstants = function () {
        return {
                apiURL: '//localhost:8005/v1'
        };
    };

    app.factory("JsavConstants", JsavConstants);

}());