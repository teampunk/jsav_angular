/**
* LoginController
* @namespace thinkster.authentication.controllers
*/
(function () {
  'use strict';
  angular
    .module('jsav.authentication.controllers', ['ngMessages'])
    .controller('LoginController', LoginController);
  	

  	LoginController.$inject = ['$location', '$scope', 'Authentication', 'Validate', '$mdDialog','$window', ];

    function LoginController($location, $scope, Authentication, Validate,$mdDialog, $window){
    	var vm = this;
    	vm.login = login;
    	vm.showAdvanced = showAdvanced;
    	// vm.cancel = cancel;
    	vm.object = {};


	    activate();
	/**
	    * @name activate
	    * @desc Actions to be performed when this controller is instantiated
	    * @memberOf thinkster.authentication.controllers.LoginController
	    */
	    function activate() {
			showAdvanced();
	      // If the user is authenticated, they should not be here.
	      if (!Authentication.isAuthenticated()) {
	        $location.path('/login');
	      }
	      else
	      {
	      	
	      	$location.path('/');	
	      }
	     
	    }

		     /**
	    * @name login
	    * @desc Log the user in
	    * @memberOf thinkster.authentication.controllers.LoginController
	    */
    function login (formData){
      vm.errors = [];
      Validate.form_validation(formData,vm.errors);
      if(!formData.$invalid){
	        Authentication.login(vm.event, vm.password, vm.email)
	        .then(function(data){
	        	$window.localStorage.setItem('idioma_enabled','false');
	        	console.log("login idioma_enabled");
	          	console.log($window.localStorage.getItem('idioma_enabled') );

	        },function(error){
	        	// error case
		    	if (error.status==404) {
		    		// console.log('data error 404 ');
		    	  	// console.log(error);
		    	  	vm.errors = error.data;
		    	}
	        });
      }
    }

    function showAdvanced (ev){
	    $mdDialog.show({
	      controller: DialogController,
	      templateUrl: 'views/dialog/dialog1.tmpl.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true,
	      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	      
	    })
	    .then(function(answer) {
	      $scope.status = 'You said the information was "' + answer + '".';
	    }, function() {
	      $scope.status = 'You cancelled the dialog.';
	    });
	  };

	  function DialogController($scope, $mdDialog) {
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	  };
	    
    }

})();