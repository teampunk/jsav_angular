/**
* NavbarController
* @namespace thinkster.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('jsav.layout.controllers')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$Location','$scope', 'Authentication'];

  /**
  * @namespace NavbarController
  */
  function NavbarController($Location, $scope, Authentication) {
    var vm = this;

    vm.logout = logout;

    /**
    * @name logout
    * @desc Log the user out
    * @memberOf thinkster.layout.controllers.NavbarController
    */
    function logout() {
      Authentication.unauthenticate();
     window.location = '/';
    }
  }
})();