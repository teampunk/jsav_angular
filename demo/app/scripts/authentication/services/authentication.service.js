/**
* Authentication
* @namespace thinkster.authentication.services
*/
(function () {
  'use strict';

  angular
    .module('jsav.authentication.services')
    .factory('Authentication', Authentication);

  Authentication.$inject = ['$q', '$cookies', '$http', 'JsavConstants', '$location','$window'];

  /**
  * @namespace Authentication
  * @returns {Factory}
  */
  function Authentication($q, $cookies, $http, JsavConstants, $location,$window) {
    /**
    * @name Authentication
    * @desc The Factory to be returned
    */
    var vm = this;
    var event = [];
    var apiURL = JsavConstants.apiURL;
    var Authentication = {
      getAuthenticatedAccount: getAuthenticatedAccount,
      isAuthenticated: isAuthenticated,
      login: login,
      logout: logout,
      // register: register,
      setAuthenticatedAccount: setAuthenticatedAccount,
      unauthenticate: unauthenticate,
      getLanguages:getLanguages,
      setLanguages:setLanguages,

    };

    return Authentication;
    /**
     * @name login
     * @desc Try to log in with email `email` and password `password`
     * @param {string} email The email entered by the user
     * @param {string} password The password entered by the user
     * @returns {Promise}
     * @memberOf thinkster.authentication.services.Authentication
     */
    function login(event, password, email) {
      var deferred = $q.defer()
      if (!email) {
        console.log('sin mail');
        $http.post(apiURL + '/login-event/', {
          event: event, password: password
        }).then(loginSuccessFn, loginErrorFn)
      }
      else{
        console.log('con mail');
        console.log(email);
         $http.post(apiURL + '/login-event/', {
          event: event, password: password, email: email
        }).then(loginSuccessFn, loginErrorFn)
      }
      /**
       * @name loginSuccessFn
       * @desc Set the authenticated account and redirect to index
       */
      function loginSuccessFn(data, status, headers, config, $q) {
        Authentication.setAuthenticatedAccount(data.data);
        Authentication.setLanguages('es');
        window.location = '/#/';
        deferred.resolve(data, status);
      }

      /**
       * @name loginErrorFn
       * @desc Log "Epic failure!" to the console
       */
      function loginErrorFn(data, status, headers, config) {
          deferred.reject(data, status, headers, config);
      }
      return deferred.promise;
    }

    function getAuthenticatedAccount() {
      // if (!$cookies.authenticatedAccount) {
      //   console.log('if !$cookies.authenticatedAccount');
      //   return;
      // }
      if (!$cookies.getObject('id_event')) {
        return;
      }
      
      return $cookies.getObject('id_event');
    }
        /**
     * @name isAuthenticated
     * @desc Check if the current user is authenticated
     * @returns {boolean} True is user is authenticated, else false.
     * @memberOf thinkster.authentication.services.Authentication
     */
    function isAuthenticated() {

      return !!$cookies.getObject('id_event');
      console.log($window.localStorage.getItem('id_event'));
    }
        /**
     * @name setAuthenticatedAccount
     * @desc Stringify the account object and store it in a cookie
     * @param {Object} user The account object to be stored
     * @returns {undefined}
     * @memberOf thinkster.authentication.services.Authentication
     */
    function setAuthenticatedAccount(account) {
      $cookies.putObject('id_event', account);
      $window.localStorage.setItem('id_event', account);
      // $cookies.authenticatedAccount = JSON.stringify(account);
    }
        /**
     * @name unauthenticate
     * @desc Delete the cookie where the user object is stored
     * @returns {undefined}
     * @memberOf thinkster.authentication.services.Authentication
     */
    function unauthenticate() {
      // delete $cookies.authenticatedAccount;
      $cookies.remove('id_event');
      $location.path('/login/');
      localStorage.removeItem('id_event');
      
    }
  /**
 * @name logout
 * @desc Try to log the user out
 * @returns {Promise}
 * @memberOf thinkster.authentication.services.Authentication
 */
function logout() {
  // return $http.post(apiURL + 'auth/logout/')
    // .then(logoutSuccessFn, logoutErrorFn);
    $cookies.remove('id_event');
    return 

  /**
   * @name logoutSuccessFn
   * @desc Unauthenticate and redirect to index with page reload
   */
  function logoutSuccessFn(data, status, headers, config) {
    Authentication.unauthenticate();
    // window.location = '/login';
  }

  /**
   * @name logoutErrorFn
   * @desc Log "Epic failure!" to the console
   */
  function logoutErrorFn(data, status, headers, config) {
    console.error('Epic failure!');
  }

}

  function getLanguages(){
        var language = $cookies.getObject('idioma');
        if (!language) {
          language = [];
        } else {
          // language = JSON.parse(language);
        }
        return language;
    }

  function setLanguages(selected_language){
      $cookies.remove('idioma');
      $cookies.putObject('idioma', selected_language );
      window.location = '/#/';
    }


}
})();