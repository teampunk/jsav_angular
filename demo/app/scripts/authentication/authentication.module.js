(function () {
  'use strict';

  angular
    .module('jsav.authentication', [
      'jsav.authentication.controllers',
      'jsav.authentication.services'
    ]);

  angular
    .module('jsav.authentication.controllers', ['JsavConstants', 'ngMessages']);

  angular
    .module('jsav.authentication.services', ['ngCookies']);
})();